/*
 *
 * Global configurations affecting *all*
 * tests go here.
 * This runs before loading the tests,
 * so it's the best place to run setup code
 *
 */

'use strict'

const AWS = require('aws-sdk')

// This is needed to run before anything else
// since some of the project modules are instantiating
// AWS libs as globals
const config = require('../test.config').aws
AWS.config.update(config)
process.env.INPUT_EVENT_TYPE = 's3'
process.env.OUTPUT_EVENT_TYPE = 's3'

process.env.INPUT_EVENT_TYPE = 'S3'
process.env.OUTPUT_EVENT_TYPE = 'S3'
