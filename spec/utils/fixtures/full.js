'use strict'

module.exports = {
  flow_augmenters_view: {
    campaignId: 1,
    lineId: 1,
    orderId: 1,
    clientId: 1,
    sourceId: 1,
    creativeId: 1,
    accountManagerId: 1,
    salesManagerId: 1,
    adopsManagerId: 1,
    departmentId: 1,
    labelId: 1,
    sellingModeId: 1,
    buyingModeId: 1,
    agencyId: 1,
    productId: 1,
    sellingPayout: 1,
    buyingFinancialEntityId: null,
    sellingFinancialEntityId: null,
    sourceAccountManagerId: 1,
    orderTaxes: 1,
    orderRebate: 1,
    sourceType: null,
    clientType: null,
    lineType: null,
    hash: '5b62564'
  }
}