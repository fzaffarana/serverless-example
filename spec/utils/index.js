'use strict'

module.exports = {
  fixtures: require('./fixtures'),
  database: require('./database'),
  aws: require('./aws')
}
