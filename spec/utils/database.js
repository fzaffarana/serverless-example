'use strict'

/**
 * Resolve dependency for just one table
 *
 * @param {string} tableName - Name of the table to be resolved
 * @param {object} tables - A PJO containing tables,
 *   fields, values and FKs.
 * @param {object} resolved - A PJO for tracking the
 *   resolved dependencies so far
 * @return {Array} - List of tables ordered by dependency
 * @private
 */
function _resolveTablesOrder (tableName, tables, resolved) {
  if (resolved[tableName]) {
    return []
  }

  var table = tables[tableName]

  if (!table) {
    return []
  }

  var resolvedTables = []

  Object.keys(table._fk || {}).forEach(function (fk) {
    var rel = table._fk[fk]
    var relTable = rel.split('.')[0]

    // Skip if already processed
    if (resolved[relTable]) {
      return
    }

    // Skip if record creation is not required
    if (typeof table[fk] === 'undefined' || table[fk] !== null) {
      return
    }

    // Skip if self related
    if (tableName === relTable) {
      return
    }

    resolvedTables = resolvedTables.concat(
      _resolveTablesOrder(relTable, tables, resolved))
  })

  resolvedTables.push(tableName)
  resolved[tableName] = true

  return resolvedTables
}

/**
 * Resolve dependency order for the given tables
 *
 * @param {object} tables - A PJO containing tables,
 *   fields, values and FKs.
 * @return {Array} - List of tables ordered by dependency
 * @private
 */
function resolveTablesOrder (tables) {
  var resolved = {}
  var resolvedTables = []

  Object.keys(tables).forEach(function (tableName) {
    resolvedTables = resolvedTables.concat(
      _resolveTablesOrder(tableName, tables, resolved))
  })

  return resolvedTables
}

/**
 * Add resolved FK IDs into a record
 *
 * This will add the FK to the record only
 * if the target field is null, meaning it
 * does not contain a resolved relationship
 *
 * Be aware this does not mutate the given record
 *
 * @param {object} record - Record fields, values and FKs.
 * @param {object} inserted - A map of resolved FKs to
 *   set the FK fields
 * @return {object} - Record with the resolved FKs
 * @private
 */
function _resolveFKs (record, inserted) {
  record = Object.assign({}, record)

  if (!record._fk) {
    return record
  }

  var fks = record._fk
  delete record._fk

  Object.keys(fks).forEach(function (field) {
    var relName = fks[field].split('.')[0]

    if (!inserted.hasOwnProperty(relName)) {
      return
    }

    if (record[field] && record[field] !== null) {
      return
    }

    record[field] = inserted[relName]
  })

  return record
}

/**
 * Insert records recursively in a given order.
 *
 * @param {int} index - Index of the record to insert.
 * @param {object} inserted - A PJO for tracking what
 *   records have been inserted so far and skipping them
 *   if they appear as FKs in multiple tables
 * @param {object} connection - MySql connection
 * @param {object} records - A PJO containing the records to be inserted
 * @param {Array} tablesOrder - List containing the order in which
 *   records should be inserted
 * @param {function} callback - Callback: function (err, recordsIds) {}
 * @private
 */
function _insert (index, inserted, connection, records, tablesOrder, callback) {
  if (index > tablesOrder.length - 1) {
    return callback(null, inserted)
  }

  var tableName = tablesOrder[index]
  var record = _resolveFKs(records[tableName], inserted)

  connection.query('INSERT INTO `' + tableName + '` SET ?', record, function (err, data) {
    if (err) {
      return callback(err)
    }

    inserted[tableName] = data.insertId
    _insert(index + 1, inserted, connection, records, tablesOrder, callback)
  })
}

/**
 * Insert a fixture into the database.
 *
 * This works by going through the given fixture
 * and following all their relationships to know
 * what record should be inserted first.
 *
 * All fields must be specified. FK should usually
 * be set to null, but if they contain a previously
 * resolved record ID then that FK lookup will be skipped.
 *
 * Be aware this will not resolve circular relationships,
 * SQL won't either. There's usually an optional value
 * (i.e NULL) in one of them, so don't add the relationships
 * for that one and set the value afterwards as it's usually done.
 *
 * The callback will receive a PJO containing
 * the names of tables and their generated PK.
 *
 * Usage:
 *   const mysqlConnection = mysql.createConnection(config);
 *   const tables = {
 *     foo: {
 *       title: 'Hey!',
 *       bar_id: null
 *       _fk: {
 *         bar_id: 'bar.id'
 *       }
 *     },
 *     bar: {
 *       name: 'Oh Noes!'
 *     }
 *   };
 *   utils.aws.database(mysqlConnection, tables, function (err, recordsIds) {
 *     expect(err).toBe(null);
 *
 *     // ...
 *   })
 *
 * @param {prototype} connection - MySQL connection.
 * @param {object} records - Plain object containing
 *   the records: table, fields, values, and relationships (FKs).
 * @param {function} callback - Callback
 * @public
 */
function insert (connection, records, callback) {
  _insert(0, {}, connection, records, resolveTablesOrder(records), callback)
}

module.exports = {
  insert: insert
}
