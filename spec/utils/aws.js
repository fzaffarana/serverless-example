'use strict'

const AWS = require('aws-sdk')

const s3 = new AWS.S3()

module.exports = {
  /**
   * Empty a given bucket.
   * This is useful for starting every test with a clean bucket.
   * Also, since the bucket must be empty to delete it.
   *
   * Usage:
   *   utils.aws.emptyBucket('My_Bucket', function (err, data) {
   *     expect(err).toBe(null);
   *     expect(data.Errors).toEqual([]);
   *
   *     // ...
   *   })
   *
   * @param {string} bucketName - Bucket name.
   * @param {function} callback - Callback.
   * @public
   */
  emptyBucket: function (bucketName, callback) {
    s3.listObjects({Bucket: bucketName}, function (err, data) {
      const objs = data.Contents.map(function (obj) {
        return {Key: obj.Key}
      })

      s3.deleteObjects({
        Bucket: bucketName,
        Delete: {Objects: objs}
      }, callback)
    })
  }
}
