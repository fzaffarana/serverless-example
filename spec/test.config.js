'use strict'

module.exports = {
  aws: {
    s3: {
      endpoint: 'http://localstack:4572'
    },
    sns: {
      endpoint: 'http://localstack:4575'
    },
    accessKeyId: 'foobar',
    secretAccessKey: 'foobar',
    region: 'us-east-1'
  }
}
