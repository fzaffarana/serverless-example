#!/bin/bash
# Check whether a port is open or not
#
# Usage: $ check_port localhost 9200

function check_port() {
  local host=${1} && shift
  local port=${1} && shift
  local retries=120
  local wait=1

  until( nc -zv "${host}" "${port}" ); do
    ((retries--))
    if [ $retries -lt 0 ]; then
      echo "Service ${host}:${port} didn't become ready in time."
      exit 1
    fi
    sleep "${wait}"
  done
  sleep "1"
}

check_port "$@"

