# Flow

You can find all the documentation in the **/docs** folder. There you'll find information about:
- Flow's detailed workflow
- Serverless deployments and permissions
- Lambda permissions
- Docker
- Tests

### \*\* IMPORTANT \*\*
Flow's first deployment requires a **strict order**. You can find the instructions under the **/docs** folder, in the *Serverless deployments and permissions* file.
