# Flow | Serverless

Here we will explain how to perform a deploy using Serverless and which permissions we will need.


## Different Stacks
Due to the many resources we need to create, and CloudFormation's limitation (up to 200 resources per stack), we needed to separate Flow's Serverless implementation into **multiple stacks**.
Each stack is stored as a folder inside `/serverless`.

### \*\* IMPORTANT \*\*
Flow's first deployment requires a **strict order**, starting from the base-stack:

#### 1) flow
It's the base stack. Creates and exports the common resources that the other stacks will need (S3, SNS, SQS).

#### 2) flow-functions
This stack contains the lambda functions, and requires the `flow` stack to be deployed first.
If we need to add or modify some functions later, **we can deploy this stack only**.

## Users and Policies

In order to deploy Flow, we will need a username (one for each stage).
This user needs two policies (one managed and one inline):

### 1) serverless-deployment-base
It's a *base policy*, that provides read-access to almost anything we may use in any serverless deployment.

```
"Action": [
    "cloudformation:Describe*",
    "cloudformation:List*",
    "cloudformation:Get*",
    "cloudformation:PreviewStackUpdate",
    "cloudwatch:Describe*",
    "cloudwatch:Get*",
    "cloudwatch:List*",
    "lambda:Get*",
    "lambda:List*",
    "ec2:Describe*",
    "s3:GetObject",
    "s3:List*",
    "iam:List*",
    "iam:Get*",
    "iam:Simulate*",
    "dynamodb:Describe*",
    "dynamodb:List*",
    "sns:ListTopics",
    "sqs:List*"
],
"Resource": "*"
```

It also provides access to the *common serverless deployments bucket* in S3

```
"Action": [
    "s3:GetObjectVersion",
    "s3:PutObject",
    "s3:CreateBucket",
    "s3:GetBucketVersioning",
    "s3:PutBucketVersioning",
    "s3:GetBucketLocation",
    "s3:PutBucketNotification",
    "s3:GetBucketNotification"
],
"Resource": [
    "arn:aws:s3:::sls-resources.mobra.in",
    "arn:aws:s3:::sls-resources.mobra.in/*"
]
```

And allows access to AWS logs

```
"Action": [
    "logs:DescribeLogStreams",
    "logs:FilterLogEvents",
    "logs:DescribeLogGroups"
],
"Resource": "arn:aws:logs:us-east-1:*:log-group::log-stream:"
```

### 2) serverless-deployment-flow-{stage} (inline policy)
This policy adds everything else, and targets specifically to Flow's resources

Cloudformation permissions:
```
"Action": [
    "cloudformation:CancelUpdateStack",
    "cloudformation:ContinueUpdateRollback",
    "cloudformation:CreateChangeSet",
    "cloudformation:ExecuteChangeSet",
    "cloudformation:CreateStack",
    "cloudformation:UpdateStack",
    "cloudformation:ValidateTemplate"
],
"Resource": "arn:aws:cloudformation:us-east-1:*:stack/flow*prod*"
```

Events Permissions (for scheduled events):
```
"Action": "events:*",
"Resource": "arn:aws:events:us-east-1:*:rule/flow-prod*"
```

S3 permissions:
```
"Action": [
    "s3:GetObject",
    "s3:GetObjectVersion",
    "s3:PutObject",
    "s3:CreateBucket",
    "s3:ListBucket",
    "s3:ListBucketVersions",
    "s3:GetBucketVersioning",
    "s3:PutBucketVersioning",
    "s3:GetBucketLocation",
    "s3:PutBucketNotification",
    "s3:GetBucketNotification"
],
"Resource": [
    "arn:aws:s3:::flow-prod.mobra.in",
    "arn:aws:s3:::flow-prod.mobra.in/*"
]
```

Lambda Permissions:
```
"Action": "lambda:*",
"Resource": "arn:aws:lambda:us-east-1:*:function:flow-prod*"
```

SNS Permissions:
```
"Action": [
    "SNS:ListSubscriptionsByTopic",
    "SNS:Subscribe",
    "SNS:Unsubscribe",
    "SNS:CreateTopic",
    "SNS:GetTopicAttributes",
    "SNS:SetTopicAttributes"
],
"Resource": "arn:aws:sns:us-east-1:*:flow-prod*"
```

SQS Permissions:
```
"Action": [
    "sqs:CreateQueue",
    "sqs:GetQueueAttributes",
    "sqs:GetQueueUrl",
    "sqs:ListDeadLetterSourceQueues",
    "sqs:ListQueues",
    "sqs:SetQueueAttributes"
],
"Resource": "arn:aws:sqs:us-east-1:*:flow-prod*"
```

Kinesis Permissions:
```
"Action": [
    "kinesis:DecreaseStreamRetentionPeriod",
    "kinesis:CreateStream",
    "kinesis:DescribeStream",
    "kinesis:ListTagsForStream",
    "kinesis:RemoveTagsFromStream",
    "kinesis:MergeShards",
    "kinesis:SplitShard",
    "kinesis:AddTagsToStream",
    "kinesis:IncreaseStreamRetentionPeriod"
],
"Resource": "arn:aws:kinesis:*:*:stream/flow-stage*"
```
```
"Action": [
    "kinesis:ListStreams",
    "kinesis:EnableEnhancedMonitoring",
    "kinesis:UpdateShardCount",
    "kinesis:DescribeLimits",
    "kinesis:DisableEnhancedMonitoring"
],
"Resource": "*"
```

IAM PassRole:
```
"Action": [
    "iam:PassRole",
    "iam:GetRole"
],
"Resource": "arn:aws:iam::*:role/lambda-flow-prod*"
```

Logs:
```
"Action": [
    "logs:*"
],
"Resource": "arn:aws:logs:us-east-1:*:*:/aws/lambda/flow-prod*:*"
```

Alarms:
```
"Action": [
    "cloudwatch:DeleteAlarms",
    "cloudwatch:PutMetricAlarm"
],
"Resource": "*"
```


## Deployments

### 1) Install *serverless* 
In order to deploy Flow, we will first need to install *serverless* using npm.

`npm install -g serverless`

Be sure you are installing the version required by the **serverless.yml** file (attribute **frameworkVersion**) 

### 2) Store credentials
In order to perform a deploy in AWS, *serverless* uses **CloudFormation**, which requires AWS credentials.
To set this up, we need to create the file **~/.aws/credentials**, and insert our credentials in it, following the next structure:

```
[{CREDENTIALS_ALIAS}]
aws_access_key_id = {ACCESS_KEY}
aws_secret_access_key = {SECRET_KEY}
```

For example:

```
[flow-prod]
aws_access_key_id = AKIAA9OPXNP5QWJHA3KE
aws_secret_access_key = QHiBQR73fjDxfrR53abB/UOdT0gTDTMMwrwo16uft
```

### 3) Set credentials
Because we may have several credentials inside that file, we now need to tell *Serverless* which credentials we want to use:

```
$ export AWS_PROFILE=flow-prod 
```

### 4) Run the *deploy* command

```
$ sls deploy [--stage prod]
```
