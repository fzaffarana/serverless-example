# Flow | Tests

## Brief

We use [jasmine test-framework][0] for making and running tests.

Tests (also called `spec`) are found within every lambda or library. Every Spec should contain just
one test suite (called `describe`), and every test suite should contain many tests cases
(called `it`).

Tests suites should test just one module. Every test case should test just one case and they
should run isolated from the rest, meaning they should never ever make another test fail/pass.
To that end, make sure to setup the pre-requirements within `beforeEach` and clean
after it within `afterEach` functions.

Be aware tests usually don't stop at the first unexpected error. They keep running.
So, an error within beforeEach/afterEach may propagate to every test case (i.e: they all fail).

Integration tests use localstack (at the time of writing anyway), which is a sort of local
AWS, however it has some shortcomings since it's not the real AWS. Make sure to check tests
from other projects ([flow][1], [cap-o][2], [workers][3] and [node-helpers][4])
if you get stuck and are looking for inspiration.

Unit tests should mock every IO request. This usually means faking some API behaviour and
hard-coding its response. Unit tests should test just one unit of code, meaning a function/method.
However, mocking too much is not desirable, otherwise you end up making the code
very resilient to changes and so the test cases pretty fragile. Mocking IO's and very
expensive operations should be enough. Also, use [mockery][5] when you find a
global and want to override it.

If time is not on your side, then prefer integration tests over unit tests. They are easier to
code and tests the full extent of the library. Downside is debugging is harder when something
goes wrong.

[0]: https://jasmine.github.io
[1]: https://github.com/HwdGroup/flow
[2]: https://github.com/HwdGroup/cap-o
[3]: https://github.com/HwdGroup/analytics-workers
[4]: https://github.com/HwdGroup/node-helpers
[5]: https://github.com/mfncooper/mockery

## Tests

Run all tests:

```
$ jasmine
```

Stop on first error:

```
$ jasmine --stop-on-failure=true
```

Run just one test:

```
$ jasmine --filter="a spec name"
```
