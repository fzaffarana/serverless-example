# Flow | Docker

## Brief

The project folder is shared between the host machine and the docker container.

`docker-compose.yml` contains the linked services (docker containers) currently in use to run
the lambdas locally (i.e: to test them).

`Dockerfile` contains the setup of the main container where the project runs.

## Install

[Install docker compose][0]

[0]: https://docs.docker.com/compose/install/

## Run

Open a terminal and run:

```sh
$ cd docker
$ docker-compose up
```

Open another terminal while keeping the first terminal opened and run:

```sh
$ docker-compose run flow /bin/bash
```

## Extra

After making changes to `Dockerfile`, run:

```sh
$ docker-compose build
```

After upgrading one of the images within `docker-compose.yml`, run:

```sh
$ docker-compose pull
```
