# Flow | Dead Letter Queue Retry

After a Lambda function execution **fails 3 times** (default "retry attempts", can be changed), we have the possibility to send that fail event into a **SQS Dead Letter Queue**.

This project has a special Lambda function, located under `/src/dlq`, that **executes every hour** and try to re-process every single event existing in that queue by **publishing into the SNS topic** that triggered the failed Lambda.

**IMPORTANT:** This behaviour requires every Lambda in this project to implement some **"duplicated execution validation"** (because some successful Lambdas may be triggered more than once).
In most cases we use **memcache** to achieve this.


## Policies

In order to deploy a scheduled Lambda, we will need to add this fragment into the Serverless user custom inline policy:

```
"Action": "events:*",
"Resource": "arn:aws:events:us-east-1:*:rule/flow-prod*"
```

And for the DLQ-Retry Lambda we need a new role (called "*lambda-flow-prod-dlq-retry*"), with the following managed policies:

#### 1) logs-create (existing and already used in other roles)

```
"Action": [
    "logs:CreateLogGroup",
    "logs:CreateLogStream",
    "logs:PutLogEvents"
],
"Resource": "arn:aws:logs:*:*:*"
```

#### 2) flow-prod-sqs-full

```
"Action": [
    "sqs:DeleteMessage",
    "sqs:DeleteMessageBatch",
    "sqs:GetQueueAttributes",
    "sqs:ListQueues",
    "sqs:ReceiveMessage"
],
"Resource": "arn:aws:sqs:us-east-1:*:flow-prod*"
```

#### 3) flow-prod-sns-publish

```
"Action": [
    "sns:ListSubscriptions",
    "sns:ListTopics",
    "sns:Publish"
],
"Resource": "arn:aws:sns:us-east-1:*:flow-prod*"
```

