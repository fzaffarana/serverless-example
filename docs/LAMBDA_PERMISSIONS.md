# Flow | Lambda Permissions

Here we will describe which policies are needed in order to get every lambda up and running.


## Roles and Policies

Flow has 3 different types of Lambdas: curators, augmenters and aggregators.
Lucky for us, all of them require the same permissions, so we created **managed policies**.

### 1) AWSLambdaVPCAccessExecutionRole (comes with AWS)
This policy will allow the lambda to communicate with other resources inside the production VPC, such as memcached servers.


### 2) flow-{stage}-sqs-dlq
Every lambda needs permission to send messages to it's own dead letter queue. This policy allows Flow lambdas to send the failed executions to the Flow DLQ.

```
"Action": [
    "sqs:SendMessage"
],
"Resource": [
    "arn:aws:sqs:us-east-1:*:flow-prod*"
]
```

### 3) flow-{stage}-s3-consumer
As its explained in <u>FLOW.md</u>, every step in Flow consists in downloading a file from S3, process it, and upload a new file to S3 into a different folder. So, we need permission to read from Flow's S3 bucket.

```
"Action": [
    "s3:GetObject"
],
"Resource": [
    "arn:aws:s3:::flow-prod.mobra.in",
    "arn:aws:s3:::flow-prod.mobra.in/*"
]
```

### 4) flow-{stage}-s3-producer
As explained in the last item, we will need to upload the output file into Flow's S3 bucket.

```
"Action": [
    "s3:AbortMultipartUpload",
    "s3:GetBucketAcl",
    "s3:GetBucketCORS",
    "s3:GetBucketLocation",
    "s3:GetBucketLogging",
    "s3:GetBucketNotification",
    "s3:GetBucketPolicy",
    "s3:GetBucketRequestPayment",
    "s3:GetBucketTagging",
    "s3:GetBucketVersioning",
    "s3:GetBucketWebsite",
    "s3:GetLifecycleConfiguration",
    "s3:GetObject",
    "s3:GetObjectAcl",
    "s3:GetObjectTorrent",
    "s3:GetObjectVersion",
    "s3:GetObjectVersionAcl",
    "s3:GetObjectVersionTorrent",
    "s3:ListAllMyBuckets",
    "s3:ListBucket",
    "s3:ListBucketMultipartUploads",
    "s3:ListBucketVersions",
    "s3:ListMultipartUploadParts",
    "s3:PutObject",
    "s3:PutObjectAcl",
    "s3:PutObjectVersionAcl",
    "s3:RestoreObject"
],
"Resource": [
    "arn:aws:s3:::flow-prod.mobra.in",
    "arn:aws:s3:::flow-prod.mobra.in/*"
]
```

### 5) flow-{stage}-sns-publish
As we are migrating from S3 path to SNS directly, we now need permission to publish to an SNS topic.

```
"Action": [
    "sns:ListSubscriptions",
    "sns:ListTopics",
    "sns:Publish"
],
"Resource": [
    "arn:aws:sns:::flow-prod*"
]
```
