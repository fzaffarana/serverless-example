# Flow

Here we will describe how Flow works, it's components and the way they are connected to each other.

## What is Flow?
**Flow** it's a system that receives data from the different tracking servers (such as impressions, clicks and conversions) and process that information in order to provide **augmented and aggregated data** to other apps.

For example, Flow receives click-stream data from the click-tracking servers, transforms that information, and aggregates it by different dimensions, so the **Analytics Workers** app can later read Flow's output and insert that data into mongo.

## Types of Lambdas

Flow has 3 different type of lambdas:
- Curators
- Augmenters
- Aggregators

### Curators
They are meant to heal and fix the **raw files**, so that they will have all the same format, and to ensure that numbers are numbers, and strings are strings.


### Augmenters
The augmenters will receive **curated files** (in most cases) and add important information that they are missing (such as GeoIP intel or whatever).

### Aggregators
The aggregators will have **curated or augmented files** as input. Their purpose is to group data by a particular set of keys. The result will be a much shorter set of data (grouped rows) that can later be used by different apps, depending on their needs.

For example, we have different aggregators for each Cube dimension (by browser, geo, OS, publisher...). That output file will be the **Analytics Workers** input.

## How is everything connected?
Flow has a complex workflow, that involves many AWS services, triggers and resources.

### 1) The raw file
Each tracking server generates a **JSON log** that contains that event information. Every 1 second (in some cases this lapse may be different) **td-agent** service will upload the new events to a **raw folder** inside Flow's bucket.

### 2) The *raw* SNS trigger
Every time a file is uploaded to the **raw folder**, an **SNS Topic** is triggered, informing that there is a new raw file, and its path inside the bucket.

### 3) The *curators* Lambdas
Once the *raw* SNS Topic is triggered, this will provoke the execution of some Lambda functions: in most cases, the **curators**. Usually only one function is attached to that specific topic, but could be more than just one.

These Lambdas will end with the creation of a new file inside the **curateds folder**.

### 4) The *curated* SNS trigger
When a file is created in the **curateds folder**, the **Curated SNS Topic** will be triggered, informing that there is a new curated file.

### 5) The *augmenters* Lambdas
This step is optional, and in some cases may occur between the **raw** and the **curated** steps. There might be **augmented Lambdas** waiting for the **Curated SNS Topic** to be triggered. As happens with the curators, in most cases only one function is attached to that specific topic, but could be more than just one.

These Lambdas will end with the creation of a new file inside the **augmenteds folder**. Remember that this step is optional, and in some cases the curated files can skip this step and trigger the aggregators instead.

### 6) The *augmented* SNS trigger
When a file is created in the **augmenteds folder**, the **Augmented SNS Topic** will be triggered, informing that there is a new augmented file.

### 7) The *aggregators* Lambdas
This step may happen **after a curation or after an augmentation** as well (it depends on each case).
So, **aggregators Lambdas** will be executed by an **SNS Topic**, and in most cases there will be several Lambdas waiting for this topic.

These Lambdas will each end with the creation of a new file inside the **aggregateds folder**, in **Flow's S3 bucket**, and will serve as input for other apps (like Analytics Workers).


>(an image illustrating all these steps would be great, right?)
