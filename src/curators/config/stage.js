'use strict'

module.exports = {
  app: {
    concurrencyLimit: 1000
  },
  aws: {
    region: 'us-east-1'
  },
  compressOutput: false,
  sns: {
    topicArn: 'arn:aws:sns:us-east-1:094103223014:flow-stage-{event}-curated'
  },
  s3: {
    bucket: 'flow-stage.mobra.in',
    outputNameRules: {
      'raw': 'curated',
      'augmented': 'curated'
    }
  },
  stats: {
    defaultTags: {
      environment: 'stage',
      application: 'stage-flow-curators'
    },
    namespace: 'stage-flow-curators'
  }
}
