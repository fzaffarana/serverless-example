'use strict'

module.exports = {
  app: {
    concurrencyLimit: 1000
  },
  aws: {
    endpoint: 'http://localstack:4572',
    accessKeyId: 'foobar',
    secretAccessKey: 'foobar',
    region: 'us-east-1'
  },
  compressOutput: false,
  sns: {
    topicArn: 'arn:aws:sns:us-east-1:123456789012:flow-docker-{event}-curated'
  },
  s3: {
    bucket: 'flow-docker.mobra.in',
    outputNameRules: {
      'raw': 'curated',
      'augmented': 'curated'
    }
  },
  stats: {
    defaultTags: {
      environment: 'docker',
      application: 'docker-flow-curators'
    },
    namespace: 'docker-flow-curators'
  }
}
