'use strict'

module.exports = {
  app: {
    concurrencyLimit: 1000
  },
  aws: {
    region: 'us-east-1'
  },
  compressOutput: true,
  sns: {
    topicArn: 'arn:aws:sns:us-east-1:094103223014:flow-beta-{event}-curated'
  },
  s3: {
    bucket: 'flow-beta.mobra.in',
    outputNameRules: {
      'raw': 'curated',
      'augmented': 'curated'
    }
  },
  stats: {
    defaultTags: {
      environment: 'beta',
      application: 'beta-flow-curators'
    },
    namespace: 'beta-flow-curators'
  }
}
