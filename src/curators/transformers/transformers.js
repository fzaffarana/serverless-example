'use strict'

module.exports = {
  floatTransformer: function (number) {
    return parseFloat(parseFloat(number).toFixed(10))
  },
  toStringTransformer: function (something) {
    return String(something)
  }
}
