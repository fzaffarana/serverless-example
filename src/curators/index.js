'use strict'

// Vendor
const environment = process.env.ENVIRONMENT || 'docker'
const inputEventType = process.env.INPUT_EVENT_TYPE
const outputEventType = process.env.OUTPUT_EVENT_TYPE
const config = require('./config/' + environment)
const compressOutput = process.env.COMPRESS_OUTPUT !== undefined ? !!process.env.COMPRESS_OUTPUT : config.compressOutput

const EventRecord = require('./modules/EventRecord')
const LambdaContext = require('node-helpers').LambdaContext
const Profiler = require('node-helpers').Profiler

const InputEventHandler = require('node-helpers')['FlowInput' + inputEventType.toUpperCase()]
const OutputEventHandler = require('node-helpers')['FlowOutput' + outputEventType.toUpperCase()]

const profiler = new Profiler({ enabled: !!Number(process.env.PROFILING_ENABLED) })

// Metrics
const NodeHelpersDDLogger = require('node-helpers').DDLogger
const DDLogger = new NodeHelpersDDLogger({
  namespace: config.stats.namespace,
  defaultTags: config.stats.defaultTags
})

exports.handler = function (event, context, cb) {
  context.callbackWaitsForEmptyEventLoop = false

  const lambdaContext = new LambdaContext(context)
  const input = new InputEventHandler(event)

  // Set config attributes for eventRecord module
  // it should be "default" or "rejected"
  const rules = process.env.VALIDATION_RULES || 'default'
  const configAttributes = require(`./eventRecordConfig/${rules}`)

  let logPrefix = `[Version: ${lambdaContext.version}]`

  // Parse the input message/data
  console.log(`${logPrefix} Parsing input data...`)
  profiler.startTimer('parse')
  const init = Date.now()
  input.parse(function inputDataParsed (err) {
    profiler.stopTimer('parse')
    DDLogger.gauge('parse_timing', Date.now() - init)

    logPrefix += `[BatchId: ${input.originalBatchId}]`

    if (err) {
      console.log(`${logPrefix} Error while parsing ${inputEventType} input`, err.stack)
      const message = `${logPrefix} An error has occurred while trying to parse the ${inputEventType} input`
      DDLogger.increment('errors', { tags: { step: 'curator', reason: 'input-parse' } })
      return cb(message)
    }

    console.log(`${logPrefix} The parsed ${inputEventType} input data size is ${input.messageSize} bytes (original: ${input.messageOriginalSize})`)
    DDLogger.gauge('input_size', input.messageSize)

    // Curate records
    console.log(`${logPrefix} Curating ${input.data.length} records`)
    const outputMessage = []
    input.data.forEach(function recordIterator (raw) {
      let record = new EventRecord(raw, configAttributes)
      outputMessage.push(record)
    })

    // Output dispatch
    profiler.startTimer('dispatch')
    console.log(`${logPrefix} Dispatching ${outputMessage.length} records`)

    const output = new OutputEventHandler(input, Object.assign({}, config[outputEventType.toLowerCase()], { compressOutput: compressOutput }))

    output.dispatch(outputMessage, function (err, result) {
      if (err) {
        console.log(err, err.stack)
        const message = `${logPrefix} An error has occurred while trying to dispatch the ${outputEventType} output`
        DDLogger.increment('errors', { tags: { step: 'curator', reason: 'dispatch' } })
        return cb(message)
      }
      DDLogger.gauge('output_size', output.messageSize)
      console.log(`${logPrefix} Successfully dispatched ${outputEventType} ${output.getOutputName()}, with size of ${output.messageSize} bytes`)
      return cb(null, result)
    })
  })
}
