'use strict'

function EventRecord (raw, configAttributes) {
  this.buildFromRaw(raw, configAttributes)
}

EventRecord.prototype.buildFromRaw = function (raw, configAttributes) {
  // Parse string to json (it may already be an object)
  try {
    raw = JSON.parse(raw)
  } catch (e) {}

  var $that = this

  // We will only accept some properties
  configAttributes.forEach(function (property) {
    if (property.required && raw[property.name] === undefined) {
      throw 'Missing field ' + property.name + ' in ' + JSON.stringify(raw)
    }

    if (raw[property.name] && raw[property.name] !== null && raw[property.name] !== '') {
      $that[property.name] = raw[property.name] = property.transformer === undefined ? raw[property.name] : property.transformer(raw[property.name], raw)
    } else {
      $that[property.name] = raw[property.name] = typeof property.default === 'function' ? property.default(raw) : property.default
    }
  })
}

module.exports = EventRecord
