'use strict'

const validRaw = '{"date":20170724,"datetime":"2017-07-24 16:40:00","campaignId":79914,"creativeId":22086,"lineId":15568,"orderId":14245,"clientId":1057,"agencyId":null,"sourceId":947,"productId":5,"buyingModeId":4,"sellingModeId":4,"departmentId":11,"accountManagerId":60,"salesManagerId":30,"adopsManagerId":20,"labelId":3,"sourceAccountManagerId":1234,"sourceType":"API,DSP,Network,Direct,Media Buyer","clientType":"Direct,Agency,Network","lineType":"Direct,Agency,Third-party,Third-party exclusive,Smartlink","categoryGroup":"mainstream","publisherId":"28_8291","adSlot":null,"siteId":null,"sellerId":null,"formatId":null,"level":"info","message":"","ip":"192.168.0.1"}'
const rawWithMissingAttributes = '{"date":20170724,"datetime":"2017-07-24 16:40:00","campaignId":79914,"creativeId":22086,"lineId":15568,"orderId":14245,"clientId":1057,"agencyId":null,"sourceId":947,"productId":5,"buyingModeId":4,"sellingModeId":4,"departmentId":11,"accountManagerId":60,"salesManagerId":30,"adopsManagerId":20,"labelId":3,"sourceAccountManagerId":1234,"sourceType":"API,DSP,Network,Direct,Media Buyer","clientType":"Direct,Agency,Network","lineType":"Direct,Agency,Third-party,Third-party exclusive,Smartlink","categoryGroup":"mainstream","publisherId":"22","adSlot":"a4b5c6d","siteId":23,"sellerId":"11","formatId":null,"level":"info","message":""}'
const rawWithRejectedAttributes = '{"date":20170724,"datetime":"2017-07-24 16:40:00","campaignId":79914,"creativeId":22086,"lineId":15568,"orderId":14245,"clientId":1057,"agencyId":null,"sourceId":947,"productId":5,"buyingModeId":4,"sellingModeId":4,"departmentId":11,"accountManagerId":60,"salesManagerId":30,"adopsManagerId":20,"labelId":3,"sourceAccountManagerId":1234,"sourceType":"API,DSP,Network,Direct,Media Buyer","clientType":"Direct,Agency,Network","lineType":"Direct,Agency,Third-party,Third-party exclusive,Smartlink","categoryGroup":"mainstream","publisherId":"28_8291","adSlot":null,"siteId":null,"sellerId":null,"formatId":null,"rejectedClicksOutOfTargeting":0,"rejectedClicksDomain":0,"rejectedClicksPublisherId":0,"rejectedClicksCapReached":1,"rejectedClicksCapReachedLifetime":0,"rejectedClicksPaused":0,"rejectedClicksFraud":0,"rejectedClicksTime":0,"rejectedClicksPlacementTransparency":0,"rejectedClicksBundleId":0,"rejectedConversionsPublisherId":0,"rejectedConversionsCapReached":0,"rejectedConversionsCapReachedLifetime":0,"rejectedConversionsPaused":0,"rejectedConversionsAttributionWindow":0,"rejectedConversionsOutOfTargeting":0,"rejectedEarn":0,"rejectedSpent":0,"level":"info","message":""}'

describe('Curators Unit EventRecord suite', () => {
  const EventRecord = require('../../modules/EventRecord')

  // For valid metric -----------------------------------------------------

  it('should return a new object', () => {
    let configAttributes = require('../../eventRecordConfig/default')
    let record = new EventRecord(validRaw, configAttributes)
    expect(typeof record).toBe('object')
  })

  it('should generate a new object with all attributes', () => {
    let configAttributes = require('../../eventRecordConfig/default')
    let record = new EventRecord(validRaw, configAttributes)
    // Record is too big to check each value count all keys
    // to ensure that nothing is missing
    expect(Object.keys(record).length).toBe(63)
  })

  it('should include buying and selling fiantial entity id', () => {
    let configAttributes = require('../../eventRecordConfig/default')
    let record = new EventRecord(validRaw, configAttributes)
    expect(record.buyingFinancialEntityId).toBe(null)
    expect(record.sellingFinancialEntityId).toBe(null)
  })

  it('should include account manager id', () => {
    let configAttributes = require('../../eventRecordConfig/default')
    let record = new EventRecord(validRaw, configAttributes)
    expect(record.sourceAccountManagerId).toBe(1234)
  })

  it('should fail if one required attributes is missing', () => {
    let configAttributes = require('../../eventRecordConfig/default')
    // toThrow expect a function to check the error
    let record = function () { new EventRecord(rawWithMissingAttributes, configAttributes) }
    expect(record).toThrow()
  })

  // For rejeced metric -----------------------------------------------------

  it('should return a smaller object for reject metric', () => {
    let configAttributes = require('../../eventRecordConfig/rejected')
    let record = new EventRecord(rawWithRejectedAttributes, configAttributes)
    expect(Object.keys(record).length).toBe(54)
  })
})
