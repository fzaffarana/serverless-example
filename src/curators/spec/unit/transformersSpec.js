'use strict'

describe('Curators Unit transformers suite', () => {
  const transformers = require('../../transformers/transformers')

  it('string transformer should return a string', () => {
    let res = transformers.toStringTransformer(4.5)
    expect(res).toBe('4.5')
  })

  it('float transformer should return a float', () => {
    let res = transformers.floatTransformer('4.5')
    expect(res).toBe(4.5)
  })
})
