'use strict'

describe('Curators Integration Test Suite', function () {
  const AWS = require('aws-sdk')
  const async = require('async')
  const curated = require('../mocks-records/curated.json')
  const stringCurated = JSON.stringify(curated)
  const fs = require('fs')

  const utils = require('../../../../spec/utils')

  const s3 = new AWS.S3()
  const sns = new AWS.SNS()
  const lambdaBaseArn = 'arn:aws:lambda:us-east-1:123456789012:function:'
  const topicName = 'foo_test'
  const bucketName = 'flow-docker.mobra.in'
  let topicArn

  const s3event = {
    Records: [{
      s3: {
        bucket: {
          name: bucketName
        },
        object: {
          key: 'conversions/raw/foo'
        }
      }
    }]
  }

  const snsEvent = {
    Records: [
      {
        EventVersion: '1.0',
        EventSource: 'aws:sns',
        Sns: {
          SignatureVersion: '1',
          Timestamp: '1970-01-01T00:00:00.000Z',
          Signature: 'EXAMPLE',
          SigningCertUrl: 'EXAMPLE',
          MessageId: '95df01b4-ee98-5cb9-9903-4c221d41eb5e',
          Message: stringCurated,
          MessageAttributes: {
            Test: {
              Type: 'String',
              Value: 'TestString'
            },
            TestBinary: {
              Type: 'Binary',
              Value: 'TestBinary'
            }
          },
          Type: 'Notification',
          UnsubscribeUrl: 'EXAMPLE',
          TopicArn: 'arn:aws:sns:us-east-1:123456789012:flow-docker-{event}-curated',
          Subject: 'TestInvoke'
        }
      }
    ]
  }

  const channel = {
    S3: 'S3',
    SNS: 'SNS'
  }

  const setEnv = (input, output) => {
    process.env.INPUT_EVENT_TYPE = input
    process.env.OUTPUT_EVENT_TYPE = output
  }

  beforeAll(function (done) {
    async.parallel([
      function (callback) {
        s3.createBucket({Bucket: bucketName}, function (err) {
          callback(null)
        })
      },
      function (callback) {
        sns.createTopic({Name: topicName}, function (err, data) {
          topicArn = data.TopicArn
          callback(null)
        })
      }
    ],
    // optional callback
      function (err, results) {
        done()
      })
  })

  afterAll(function (done) {
    async.parallel([
      function (callback) {
        utils.aws.emptyBucket(bucketName, function (err) {
          callback(null)
        })
      },
      function (callback) {
        sns.deleteTopic({TopicArn: topicArn}, function (err, data) {
          callback(null)
        })
      }
    ],
    // optional callback
      function (err, results) {
        done()
      })
  })

  afterEach(function (done) {
    delete require.cache[require.resolve('../../index.js')]
    done()
  })

  it('should curate a record stored in s3 and store result in s3', function (done) {
    setEnv(channel.S3, channel.S3)
    const curator = require('../..').handler
    fs.readFile(__dirname + '/raw', 'utf8', function (err, contents) {
      if (!err) {
        s3.putObject({Bucket: bucketName, Key: 'conversions/raw/foo', Body: contents}, function (err) {
          curator(s3event, {invokedFunctionArn: lambdaBaseArn + 'flow-docker-conversions-curator:test'}, function (err, data) {
            expect(err).toBe(null)
            s3.getObject({Bucket: bucketName, Key: 'conversions/curated/foo'}, function (err, data) {
              expect(data.Body.toString()).toBe(stringCurated)
              done()
            })
          })
        })
      }
    })
  })

  it('should curate a record stored in sns', function (done) {
    setEnv(channel.SNS, channel.SNS)
    const curator = require('../..').handler
    curator(snsEvent, {invokedFunctionArn: lambdaBaseArn + 'flow-docker-conversions-curator:test'}, function (err, data) {
      expect(err).toBe(null)
      expect(data.message).toEqual(stringCurated)
      done()
    })
  })
})
