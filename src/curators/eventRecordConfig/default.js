'use strict'

var url = require('url')
var tools = require('node-helpers').tools
var transformers = require('../transformers/transformers')

module.exports = [
  // Click info
  {
    name: 'id',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  },
    /* {
     name: 'version',
     required: false,
     default: 1,
     transformer: parseInt
     }, */
    /* {
     name: 'param1',
     required: false,
     default: null,
     transformer: transformers.toStringTransformer
     },
     {
     name: 'param2',
     required: false,
     default: null,
     transformer: transformers.toStringTransformer
     },
     {
     name: 'param3',
     required: false,
     default: null,
     transformer: transformers.toStringTransformer
     },
     {
     name: 'param4',
     required: false,
     default: null,
     transformer: transformers.toStringTransformer
     },
     {
     name: 'param5',
     required: false,
     default: null,
     transformer: transformers.toStringTransformer
     },
     {
     name: 'param6',
     required: false,
     default: null,
     transformer: transformers.toStringTransformer
     }, */
  // Campaign attributes
  {
    name: 'campaignId',
    required: true,
    transformer: parseInt
  }, {
    name: 'creativeId',
    required: true,
    transformer: parseInt
  }, {
    name: 'lineId',
    required: true,
    transformer: parseInt
  }, {
    name: 'orderId',
    required: true,
    transformer: parseInt
  }, {
    name: 'clientId',
    required: true,
    transformer: parseInt
  }, {
    name: 'agencyId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'sourceId',
    required: true,
    transformer: parseInt
  }, {
    name: 'productId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'buyingModeId',
    required: true,
    transformer: parseInt
  }, {
    name: 'sellingModeId',
    required: true,
    transformer: parseInt
  }, {
    name: 'departmentId',
    required: true,
    transformer: parseInt
  }, {
    name: 'accountManagerId',
    required: true,
    transformer: parseInt
  }, {
    name: 'salesManagerId',
    required: true,
    transformer: parseInt
  }, {
    name: 'adopsManagerId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'labelId',
    required: true,
    transformer: parseInt
  }, {
    name: 'buyingFinancialEntityId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'sellingFinancialEntityId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'sourceAccountManagerId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'sourceType',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  }, {
    name: 'clientType',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  }, {
    name: 'lineType',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  },
  {
    name: 'sourceUid',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  },
  // Numbers
  {
    name: 'impressions',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'clicks',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'conversions',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'spent',
    required: false,
    default: 0,
    transformer: transformers.floatTransformer
  }, {
    name: 'earn',
    required: false,
    default: 0,
    transformer: transformers.floatTransformer
  }, {
    name: 'rebate',
    required: false,
    default: 0,
    transformer: transformers.floatTransformer
  }, {
    name: 'taxes',
    required: false,
    default: 0,
    transformer: transformers.floatTransformer
  }, {
    name: 'share',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'earlyConversions',
    required: false,
    default: function (record) { return record.earlyConversion ? 1 : 0 },
    transformer: parseInt
  }, {
    name: 'injectionConversions',
    required: false,
    default: function (record) { return record.injectionConversion ? 1 : 0 },
    transformer: parseInt
  }, {
    name: 'skippedConversions',
    required: false,
    default: function (record) { return record.skipPublisherCallback ? 1 : 0 },
    transformer: parseInt
  }, {
    name: 'minutesBetweenClicksAndConversions',
    required: false,
    default: function (record) { return record.minutesBetweenClickAndConversion ? parseInt(record.minutesBetweenClickAndConversion) : 0 },
    transformer: parseInt
  },
  // Geo
  {
    name: 'countryName',
    required: false,
    default: null
  }, {
    name: 'country',
    required: false,
    default: function (record) { return record.countryName }
  }, {
    name: 'countryCode',
    required: false,
    default: null
  }, {
    name: 'region',
    required: false,
    default: null
  }, {
    name: 'city',
    required: false,
    default: null
  }, {
    name: 'latitude',
    required: false,
    default: null
  }, {
    name: 'longitude',
    required: false,
    default: null
  }, {
    name: 'isp',
    required: false,
    default: function (record) { return record.carrier ? record.carrier : null }
  }, {
    name: 'connectionType',
    required: false,
    default: null
  },
  // UA Parser
  {
    name: 'browser',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  }, {
    name: 'browserVersion',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  }, {
    name: 'os',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  }, {
    name: 'osVersion',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  }, {
    name: 'device',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  }, {
    name: 'deviceType',
    required: false,
    default: 'desktop'
  },
  // Publisher,
  {
    name: 'domain',
    required: false,
    default: function (record) { return record.referer ? String(url.parse(String(record.referer)).hostname) : null },
    transformer: transformers.toStringTransformer
  }, {
    name: 'bundleId',
    required: false,
    default: function (record) { return record.bundleId ? transformers.toStringTransformer(record.bundleId) : null },
    transformer: transformers.toStringTransformer
  }, {
    name: 'publisherId',
    required: false,
    default: function (record) { return record.publisher ? transformers.toStringTransformer(record.publisher) : null },
    transformer: transformers.toStringTransformer
  }, {
    name: 'categoryGroup',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  }, {
    name: 'adSlot',
    required: false,
    default: null,
    transformer: function (value) { return tools.padleft(value, 7) }
  }, {
    name: 'siteId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'sellerId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'formatId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'datetime',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  }, {
    name: 'date',
    required: false,
    default: function (record) {
      var date = new Date(record.datetime)
      return parseInt(transformers.toStringTransformer(date.getFullYear()) + tools.padleft(date.getMonth() + 1, 2) + tools.padleft(date.getDate(), 2))
    },
    transformer: function (value) {
      var date = parseInt(value)
      if (String(value).match(/^(\d{4})\-(\d{2})\-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/)) {
        date = new Date(value)
        date = parseInt(transformers.toStringTransformer(date.getFullYear()) + tools.padleft(date.getMonth() + 1, 2) + tools.padleft(date.getDate(), 2))
      }
      return date
    }
  }, {
    name: 'hour',
    required: false,
    default: function (record) {
      var date = new Date(record.datetime)
      return parseInt(date.getHours())
    }
  }, {
    name: 'weekNumber',
    required: false,
    default: function (record) {
      return parseInt(tools.getWeekNumber(transformers.toStringTransformer(record.date)))
    },
    transformer: parseInt
  },
  // Request
  {
    name: 'ip',
    required: true,
    default: null,
    transformer: transformers.toStringTransformer
  }
  /*
  , {
  name: 'xForwardedFor',
  required: false,
  default: null,
  transformer: transformers.toStringTransformer
  }, {
  name: 'referer',
  required: false,
  default: null,
  transformer: transformers.toStringTransformer
  }, {
  name: 'userAgent',
  required: true,
  default: null,
  transformer: transformers.toStringTransformer
  } */
]
