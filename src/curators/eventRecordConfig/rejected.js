'use strict'

var tools = require('node-helpers').tools
var transformers = require('../transformers/transformers')

module.exports = [
  {
    name: 'campaignId',
    required: true,
    transformer: parseInt
  }, {
    name: 'creativeId',
    required: true,
    transformer: parseInt
  }, {
    name: 'lineId',
    required: true,
    transformer: parseInt
  }, {
    name: 'orderId',
    required: true,
    transformer: parseInt
  }, {
    name: 'clientId',
    required: true,
    transformer: parseInt
  }, {
    name: 'agencyId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'sourceId',
    required: true,
    transformer: parseInt
  }, {
    name: 'productId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'buyingModeId',
    required: true,
    transformer: parseInt
  }, {
    name: 'sellingModeId',
    required: true,
    transformer: parseInt
  }, {
    name: 'departmentId',
    required: true,
    transformer: parseInt
  }, {
    name: 'accountManagerId',
    required: true,
    transformer: parseInt
  }, {
    name: 'salesManagerId',
    required: true,
    transformer: parseInt
  }, {
    name: 'adopsManagerId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'labelId',
    required: true,
    transformer: parseInt
  }, {
    name: 'buyingFinancialEntityId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'sellingFinancialEntityId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'sourceAccountManagerId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'sourceType',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  },{
    name: 'clientType',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  },{
    name: 'lineType',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  },{
    name: 'publisherId',
    required: false,
    default: function (record) { return record.publisher ? transformers.toStringTransformer(record.publisher) : null },
    transformer: transformers.toStringTransformer
  }, {
    name: 'categoryGroup',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  }, {
    name: 'adSlot',
    required: false,
    default: null,
    transformer: function (value) { return tools.padleft(value, 7) }
  }, {
    name: 'siteId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'sellerId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'formatId',
    required: false,
    default: null,
    transformer: parseInt
  }, {
    name: 'datetime',
    required: false,
    default: null,
    transformer: transformers.toStringTransformer
  }, {
    name: 'date',
    required: false,
    default: function (record) {
      var date = new Date(record.datetime)
      return parseInt(transformers.toStringTransformer(date.getFullYear()) + tools.padleft(date.getMonth() + 1, 2) + tools.padleft(date.getDate(), 2))
    },
    transformer: function (value) {
      var date = parseInt(value)
      if (String(value).match(/^(\d{4})\-(\d{2})\-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/)) {
        date = new Date(value)
        date = parseInt(transformers.toStringTransformer(date.getFullYear()) + tools.padleft(date.getMonth() + 1, 2) + tools.padleft(date.getDate(), 2))
      }
      return date
    }
  }, {
    name: 'hour',
    required: false,
    default: function (record) {
      var date = new Date(record.datetime)
      return parseInt(date.getHours())
    }
  }, {
    name: 'weekNumber',
    required: false,
    default: function (record) {
      return parseInt(tools.getWeekNumber(transformers.toStringTransformer(record.date)))
    },
    transformer: parseInt
  }, {
    name: 'earlyConversions',
    required: false,
    default: function (record) { return record.earlyConversion ? 1 : 0 },
    transformer: parseInt
  }, {
    name: 'injectionConversions',
    required: false,
    default: function (record) { return record.injectionConversion ? 1 : 0 },
    transformer: parseInt
  },
  // Numbers
  {
    name: 'spent',
    required: false,
    default: 0,
    transformer: transformers.floatTransformer
  }, {
    name: 'rejectedClicksOutOfTargeting',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedClicksDomain',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedClicksPublisherId',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedClicksCapReached',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedClicksCapReachedLifetime',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedClicksPaused',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedClicksFraud',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedClicksTime',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedClicksPlacementTransparency',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedClicksBundleId',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedConversionsPublisherId',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedConversionsCapReached',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedConversionsCapReachedLifetime',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedConversionsPaused',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedConversionsMinAttributionWindow',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedConversionsMaxAttributionWindow',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedConversionsAttributionWindow',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedConversionsOutOfTargeting',
    required: false,
    default: 0,
    transformer: parseInt
  }, {
    name: 'rejectedEarn',
    required: false,
    default: 0,
    transformer: transformers.floatTransformer
  }, {
    name: 'rejectedSpent',
    required: false,
    default: 0,
    transformer: transformers.floatTransformer
  }
]
