'use strict'

module.exports = {
  app: {
    concurrencyLimit: 100
  },
  aws: {
    region: 'us-east-1'
  },
  cache: {
    options: {
      reconnect: 30000,
      timeout: 1000,
      retries: 1
    },
    prefix: 'flow_augmenters_beta_',
    ttl: 300,
    hosts: ['lambda.memcached.mobra.in:11211'],
    enabled: true
  },
  mysql: {
    host: 'clicks.mysql.mobra.in',
    user: 'content-server',
    password: 'yVxXqSVRDN3zeRqEXbFa',
    database: 'hwd',
    supportBigNumbers: true,
    charset: 'utf8'
  },
  compressOutput: false,
  sns: {
    topicArn: 'arn:aws:sns:us-east-1:094103223014:flow-beta-{event}-augmented'
  },
  s3: {
    bucket: 'flow-beta.mobra.in',
    outputNameRules: {
      'raw': 'augmented',
      'curated': 'augmented'
    }
  },
  stats: {
    defaultTags: {
      environment: 'beta',
      application: 'beta-flow-augmenters'
    },
    namespace: 'beta-flow-augmenters'
  }
}
