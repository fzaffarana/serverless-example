'use strict'

module.exports = {
  app: {
    concurrencyLimit: 1000
  },
  aws: {
    endpoint: 'http://localstack:4572',
    accessKeyId: 'foobar',
    secretAccessKey: 'foobar',
    region: 'us-east-1'
  },
  cache: {
    options: {
      reconnect: 30000,
      timeout: 1000,
      retries: 1
    },
    prefix: 'augmenters_tests_',
    ttl: 30,
    hosts: ['memcached:11211'],
    enabled: true
  },
  mysql: {
    host: 'mysql',
    user: 'root',
    password: 'root_password',
    database: 'hwd_test',
    supportBigNumbers: true
  },
  compressOutput: false,
  sns: {
    topicArn: 'arn:aws:sns:us-east-1:094103223014:flow-docker-{event}-augmented'
  },
  s3: {
    bucket: 'flow-docker.mobra.in',
    outputNameRules: {
      'raw': 'augmented',
      'curated': 'augmented'
    }
  },
  stats: {
    defaultTags: {
      environment: 'docker',
      application: 'docker-flow-augmenters'
    },
    namespace: 'docker-flow-augmenters'
  }
}
