'use strict'

const advModeMap = {
  1: 'CPC',
  2: 'CPA',
  3: 'CPM',
  4: 'CPI',
  5: 'DCPM',
  6: 'DCPC',
  7: 'RS',
  8: 'DCPA'
}

/**
 * Exception for records without winner
 *
 * @param {string} message - Message
 * @public
 */
function NoWinnerError (message) {
  if (!(this instanceof NoWinnerError)) {
    return new NoWinnerError(message)
  }

  this.name = 'NoWinnerError'

  Error.call(this, message || 'Record has no "winner"')
}

Object.setPrototypeOf(NoWinnerError.prototype, Error.prototype)

module.exports = {

  errors: {
    NoWinnerError
  },

  /**
   * A task that do some params validations and
   * passes internal params to following tasks
   *
   * @param {object} params - Parameters
   * @param {object} params.record - Record to process
   * @param {prototype} params.cache - Cache instance
   * @param {prototype} params.campaignsConfiguration - CampaignsConfiguration instance
   * @param {function} next - callback: function (err, params) {}
   * @public
   */
  init: function (params, next) {
    if (!params.record.winner) {
      return next(NoWinnerError())
    }

    return next(null, {
      found: false,
      key: `campaign-${params.record.winner}`,
      record: params.record,
      cache: params.cache,
      campaignsConfiguration: params.campaignsConfiguration
    })
  },
  /**
   * A task for fetching the missing
   * record fields from the cache
   *
   * It mutates the record
   *
   * @param {object} params - Parameters
   * @param {object} params.record - Record to process
   * @param {prototype} params.cache - Cache instance
   * @param {boolean} params.found - Indicates whether the
   *   record was found by a previous task
   *   and should skip this one or not
   * @param {string} params.key - The record key to lookup the cache
   * @param {function} next - callback: function (err, params) {}
   * @public
   */
  fetchFromCache: function (params, next) {
    if (params.found) {
      return next(null, params)
    }

    params.cache.get(params.key, function (err, campaign) {
      if (err) {
        console.log(`Find campaign from cache failed: ${err}`)
        return next(null, params)
      }

      if (typeof campaign === 'undefined') {
        return next(null, params)
      }

      // console.log('Found in cache')
      params.found = true
      Object.assign(params.record, campaign)
      return next(null, params)
    })
  },
  /**
   * A task for fetching the missing
   * record fields from the database
   *
   * It mutates the record
   *
   * @param {object} params - Parameters
   * @param {object} params.record - Record to process
   * @param {prototype} params.campaignsConfiguration - CampaignsConfiguration instance
   * @param {boolean} params.found - Indicates whether the
   *   record was found by a previous task
   *   and should skip this one or not
   * @param {string} params.key - The record key to save the record in cache
   * @param {function} next - callback: function (err, params) {}
   * @public
   */
  fetchFromDatabase: function (params, next) {
    if (params.found) {
      return next(null, params)
    }

    params.campaignsConfiguration.fetch(params.record.winner, function (err, campaign) {
      if (err) {
        console.log(err)
        return next(err)
      }

      // console.log('Found in db')
      params.found = true
      Object.assign(params.record, campaign)

      params.cache.set(params.key, campaign, null, err => { if (err) console.log(err) })

      return next(null, params)
    })
  },
  /**
   * A task for re-calculating the ern for the CPM special case
   *
   * It mutates the record
   *
   * @param {object} params - Parameters
   * @param {object} params.record - Record to process
   * @param {function} next - callback: function (err, params) {}
   * @public
   */
  calculateEarn: function (params, next) {
    if (!params.record.earn && advModeMap[params.record.sellingModeId] === 'CPM') {
      params.record.earn = params.record.sellingPayout / 1000
    }

    return next(null, params)
  },
  /**
   * A task for re-calculating the rebate for the CPM special case
   *
   * It mutates the record
   *
   * @param {object} params - Parameters
   * @param {object} params.record - Record to process
   * @param {function} next - callback: function (err, params) {}
   * @public
   */
  calculateRebate: function (params, next) {
    const rebatePercent = params.record.orderRebate || params.record.agencyRebate || 0

    if (!params.record.rebate && advModeMap[params.record.sellingModeId] === 'CPM') {
      params.record.rebate = rebatePercent && params.record.earn ? (params.record.earn * rebatePercent / 100) : 0
    }

     return next(null, params)
  },
  /**
   * A task for re-calculating the taxes for the CPM special case
   *
   * It mutates the record
   *
   * @param {object} params - Parameters
   * @param {object} params.record - Record to process
   * @param {function} next - callback: function (err, params) {}
   * @public
   */
  calculateTaxes: function (params, next) {
    if (!params.record.taxes && advModeMap[params.record.sellingModeId] === 'CPM') {
      params.record.taxes = params.record.orderTaxes && params.record.earn ? ((params.record.earn - params.record.rebate) * (params.record.orderTaxes / 100)) : 0
    }

    return next(null, params)
  }
}
