'use strict'

const mysql = require('mysql')

/**
 * Exception for record not
 * found in the database
 *
 * @param {string} message - Message
 * @public
 */
function NotFoundError (message) {
  if (!(this instanceof NotFoundError)) {
    return new NotFoundError(message)
  }

  Error.call(this, message || 'Record not found')

  this.name = 'NotFoundError'
  this.message = message
}

Object.setPrototypeOf(NotFoundError.prototype, Error.prototype)

/**
 * Exception for more than one record
 * found in the database
 *
 * @param {string} message - Message
 * @public
 */
function FoundTooManyError (message) {
  if (!(this instanceof FoundTooManyError)) {
    return new FoundTooManyError(message)
  }

  Error.call(this, message || 'Found too many records')

  this.name = 'FoundTooManyError'
  this.message = message
}

Object.setPrototypeOf(FoundTooManyError.prototype, Error.prototype)

/**
 * Common campaigns operations
 *
 * @param {object} connection - MySql connection instance
 * @member {prototype} _connection - MySQL connection instance
 * @public
 */
function CampaignsConfiguration (connection) {
  if (!(this instanceof CampaignsConfiguration)) {
    return new CampaignsConfiguration(connection)
  }

  this._connection = connection
}

/**
 * Normalize property names for a fetched campaign
 *
 * @param {object} record - Raw record fetched from the database
 * @private
 */
CampaignsConfiguration.prototype._normalize = function (record) {
  return {
    campaignId: record.campaignId,
    lineId: record.lineId,
    orderId: record.orderId,
    clientId: record.clientId,
    sourceId: record.sourceId,
    creativeId: record.creativeId,
    accountManagerId: record.accountManagerId,
    salesManagerId: record.salesManagerId,
    adopsManagerId: record.adopsManagerId,
    departmentId: record.departmentId,
    labelId: record.labelId,
    sellingModeId: record.sellingModeId,
    buyingModeId: record.buyingModeId,
    agencyId: record.agencyId,
    productId: record.productId,
    sellingPayout: record.sellingPayout,
    buyingFinancialEntityId: record.buyingFinancialEntityId,
    sellingFinancialEntityId: record.sellingFinancialEntityId,
    sourceAccountManagerId: record.sourceAccountManagerId,
    sourceType: record.sourceType || null,
    clientType: record.clientType || null,
    lineType: record.lineType || null
  }
}

/**
 * Fetch the campaign by a given hash.
 * The passed result will only contain the
 * field require for augmenting/completing
 * the record for impressions counting
 *
 * The error passed to the callback may be
 * NotFoundError, FoundTooManyError or
 * an internal mysql error
 *
 * @param {string} campaignHash - Unique hash for fetching
 *   the stored record matching `campaigns.hash` table-field
 * @param {function} callback - Callback: function (err, record) {}
 * @public
 */
CampaignsConfiguration.prototype.fetch = function (campaignHash, callback) {

    this._connection.query(
      'SELECT * FROM `flow_augmenters_view` ' +
      'WHERE hash = ? ' +
      'LIMIT 1',
      [campaignHash],
      (err, results) => {
      if (err) {
        return callback(err)
      }

    if (!results.length) {
      return callback(NotFoundError(`${campaignHash} not found in database`))
    }

    // 'campaigns.hash' is unique so this can only
    // happen if there is an error within the query
    if (results.length > 1) {
      return callback(FoundTooManyError(`Too many records found for ${campaignHash}`))
    }

    return callback(null, this._normalize(results[0]))
  })
}

module.exports = {
  CampaignsConfiguration,
  errors: {
    NotFoundError,
    FoundTooManyError
  }
}
