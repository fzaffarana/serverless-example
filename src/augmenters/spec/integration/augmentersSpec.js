'use strict'

const async = require('async');

describe('Augmenters Integration Test Suite', function () {
  jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000
  process.env.CACHE = '1'
  process.env.ENVIRONMENT = 'docker'

  const CacheDriver = require('memcached')
  const AWS = require('aws-sdk')
  const Cache = require('node-helpers').Cache
  const config = require('../../config/docker')
  const dbmigrateConfig = require('../../../../database.json').dev
  const record = require('../mocks/record.json')
  const utils = require('../../../../spec/utils')
  const MySqlConnection = require('node-helpers').MysqlConnection
  const DBMigrate = require('db-migrate')

  const migrateConfigNoDb = Object.assign({}, dbmigrateConfig, {database: null})
  const configNoDb = Object.assign({}, config.mysql, {database: null})

  const cache = new Cache(config.cache)
  const cacheDriver = new CacheDriver(config.cache.hosts, config.cache.options)

  const s3 = new AWS.S3()
  const bucketName = 'flow-docker.mobra.in'
  const dbmigrate = DBMigrate.getInstance(true)

  const cacheKey = `campaign-${record.winner}`
  const eventBody = JSON.stringify(record)
  const event = {
    Records: [{
      Sns: {
        Message: JSON.stringify({
          Records: [{
            s3: {
              bucket: {
                name: bucketName
              },
              object: {
                key: 'flow/raw/l337'
              }
            }
          }]
        })
      }
    }]
  }
  let dummyRecords
  let mysqlConnection

  beforeAll(function (done) {
    s3.createBucket({Bucket: bucketName}, function (err) {
      expect(err).toBe(null)
      const dbmigrateNoDb = DBMigrate.getInstance(true, migrateConfigNoDb)
      dbmigrateNoDb.createDatabase(config.mysql.database).then(() => {
        dbmigrate.up().then(() => {
          mysqlConnection = require('../..').testsHelpers.mysqlConnection
          done()
        }).catch((err) => {
          console.log(err)
        })
      })
    })
  })

  afterAll(function (done) {
    s3.deleteBucket({Bucket: bucketName}, function (err) {
      expect(err).toBe(null)
      dbmigrate.dropDatabase(config.mysql.database).then(() => {
        done()
      })
    })
  })

  beforeEach(function (done) {
    // Be aware all queries inside of a transaction must be on the same connection.
    // So, use or set a unique connection within the app.

    mysqlConnection = require('../..').testsHelpers.mysqlConnection

    mysqlConnection.beginTransaction(function (err) {
      if (err) {
        console.log(err)
      }
      expect(err).toBe(null)
      utils.database.insert(mysqlConnection, utils.fixtures.full, function (err, data) {
        if (err) {
          console.log(err)
        }
        expect(err).toBe(null)
        s3.putObject({Bucket: bucketName, Key: 'flow/raw/l337', Body: eventBody}, function (err) {
          expect(err).toBe(null)
          done()
        })
      })
   })
  })

  afterEach(function (done) {
    mysqlConnection.rollback(function () {
      cacheDriver.flush(function (err) {
        expect(err).not.toBeDefined()

        utils.aws.emptyBucket(bucketName, function (err, data) {
          expect(err).toBe(null)
          expect(data.Errors).toEqual([])

          done()
        })
      })
    })
  })

  afterEach(function (done) {
    delete require.cache[require.resolve('../../index.js')]
    done()
  })

  it('should augment the record going the happy path', function (done) {
    process.env.INPUT_EVENT_TYPE = 'S3'
    process.env.OUTPUT_EVENT_TYPE = 'S3'
    const augmenter = require('../..').handler
    augmenter(event, {}, function (err) {
      expect(err).toBe(null)

      s3.getObject({Bucket: bucketName, Key: 'flow/augmented/l337'}, function (err, obj) {
        expect(err).toBe(null)
        const augmentedRecord = JSON.parse(String(obj.Body))
        expect(augmentedRecord.data[0]).toEqual(Object.assign({}, record, {
          campaignId: utils.fixtures.full.flow_augmenters_view.campaignId,
          lineId: utils.fixtures.full.flow_augmenters_view.lineId,
          orderId: utils.fixtures.full.flow_augmenters_view.orderId,
          clientId: utils.fixtures.full.flow_augmenters_view.clientId,
          sourceId: utils.fixtures.full.flow_augmenters_view.sourceId,
          creativeId: utils.fixtures.full.flow_augmenters_view.creativeId,
          accountManagerId: utils.fixtures.full.flow_augmenters_view.accountManagerId,
          salesManagerId: utils.fixtures.full.flow_augmenters_view.salesManagerId,
          adopsManagerId: utils.fixtures.full.flow_augmenters_view.adopsManagerId,
          departmentId: utils.fixtures.full.flow_augmenters_view.departmentId,
          labelId: utils.fixtures.full.flow_augmenters_view.labelId,
          sellingModeId: utils.fixtures.full.flow_augmenters_view.sellingModeId,
          buyingModeId: utils.fixtures.full.flow_augmenters_view.buyingModeId,
          agencyId: utils.fixtures.full.flow_augmenters_view.agencyId,
          productId: utils.fixtures.full.flow_augmenters_view.productId,
          sellingPayout: 1,
          buyingFinancialEntityId: null,
          sellingFinancialEntityId: null,
          sourceAccountManagerId: utils.fixtures.full.flow_augmenters_view.sourceAccountManagerId,
          sourceType: utils.fixtures.full.flow_augmenters_view.sourceType,
          clientType: utils.fixtures.full.flow_augmenters_view.clientType,
          lineType: utils.fixtures.full.flow_augmenters_view.lineType
        }))
        done()
      })
    })
  })

  it('should augment the record from the cache', function (done) {
    process.env.OUTPUT_EVENT_TYPE = 'S3'
    const augmenter = require('../..').handler
    cache.set(cacheKey, {campaignId: 1337, foo: 'bar'}, null, function (err) {
      expect(err).not.toBeDefined()

      augmenter(event, {}, function (err, message) {
        expect(err).toBe(null)

        s3.getObject({Bucket: bucketName, Key: 'flow/augmented/l337'}, function (err, obj) {
          expect(err).toBe(null)

          const augmentedRecord = JSON.parse(String(obj.Body))
          expect(augmentedRecord.data[0]).toEqual(Object.assign({}, record, {campaignId: 1337, foo: 'bar'}))

          done()
        })
      })
    })
  })

  it('should save the db record into the cache', function (done) {
    process.env.OUTPUT_EVENT_TYPE = 'S3'
    const augmenter = require('../..').handler
    augmenter(event, {}, function (err, message) {
      expect(err).toBe(null)

      cache.get(cacheKey, function (err, value) {
        expect(err).not.toBeDefined()
        expect(value).toEqual({
          campaignId: utils.fixtures.full.flow_augmenters_view.campaignId,
          lineId: utils.fixtures.full.flow_augmenters_view.lineId,
          orderId: utils.fixtures.full.flow_augmenters_view.orderId,
          clientId: utils.fixtures.full.flow_augmenters_view.clientId,
          sourceId: utils.fixtures.full.flow_augmenters_view.sourceId,
          creativeId: utils.fixtures.full.flow_augmenters_view.creativeId,
          accountManagerId: utils.fixtures.full.flow_augmenters_view.accountManagerId,
          salesManagerId: utils.fixtures.full.flow_augmenters_view.salesManagerId,
          adopsManagerId: utils.fixtures.full.flow_augmenters_view.adopsManagerId,
          departmentId: utils.fixtures.full.flow_augmenters_view.departmentId,
          labelId: utils.fixtures.full.flow_augmenters_view.labelId,
          sellingModeId: utils.fixtures.full.flow_augmenters_view.sellingModeId,
          buyingModeId: utils.fixtures.full.flow_augmenters_view.buyingModeId,
          agencyId: utils.fixtures.full.flow_augmenters_view.agencyId,
          productId: utils.fixtures.full.flow_augmenters_view.productId,
          sellingPayout: utils.fixtures.full.flow_augmenters_view.sellingPayout,
          buyingFinancialEntityId: utils.fixtures.full.flow_augmenters_view.buyingFinancialEntityId,
          sellingFinancialEntityId: utils.fixtures.full.flow_augmenters_view.sellingFinancialEntityId,
          sourceAccountManagerId: utils.fixtures.full.flow_augmenters_view.sourceAccountManagerId,
          sourceType: utils.fixtures.full.flow_augmenters_view.sourceType,
          clientType: utils.fixtures.full.flow_augmenters_view.clientType,
          lineType: utils.fixtures.full.flow_augmenters_view.lineType
        })

        done()
      })
    })
  })

  it('should skip bad campaigns hashes', function (done) {
    process.env.OUTPUT_EVENT_TYPE = 'S3'
    const augmenter = require('../..').handler
    const auxEventBody = JSON.stringify(Object.assign({}, record, {winner: 'badWinner'}))

    s3.putObject({Bucket: bucketName, Key: 'flow/raw/l337', Body: auxEventBody}, function (err) {
      expect(err).toBe(null)

      augmenter(event, {}, function (err, message) {
        expect(err).not.toBeDefined()

        done()
      })
    })
  })

  it('should calculate earn for CPM', function (done) {
    process.env.OUTPUT_EVENT_TYPE = 'S3'
    const augmenter = require('../..').handler
    const auxEventBody = JSON.stringify(Object.assign({}, record, {earn: null}))
    const query = 'UPDATE `flow_augmenters_view` SET sellingPayout = 5, sellingModeId = 3 WHERE lineId = ?'

    mysqlConnection.query(query, [utils.fixtures.full.flow_augmenters_view.lineId], function (err, data) {
      expect(err).toBe(null)
      s3.putObject({Bucket: bucketName, Key: 'flow/raw/l337', Body: auxEventBody}, function (err) {
        expect(err).toBe(null)

        augmenter(event, {}, function (err, message) {
          expect(err).toBe(null)

          s3.getObject({Bucket: bucketName, Key: 'flow/augmented/l337'}, function (err, obj) {
            expect(err).toBe(null)

            const augmentedRecord = JSON.parse(String(obj.Body))
            expect(augmentedRecord.data[0].earn).toEqual(0.005)

            done()
          })
        })
      })
    })
  })

  it('should calculate taxes for CPM', function (done) {
    process.env.OUTPUT_EVENT_TYPE = 'S3'
    const augmenter = require('../..').handler
    const auxEventBody = JSON.stringify(Object.assign({}, record, {earn: 5, orderRebate: 10, orderTaxes: 10}))
    const query = 'UPDATE `flow_augmenters_view` SET sellingModeId = 3 WHERE lineId = ?'

    mysqlConnection.query(query, [utils.fixtures.full.flow_augmenters_view.lineId], function (err, data) {
      expect(err).toBe(null)
      s3.putObject({Bucket: bucketName, Key: 'flow/raw/l337', Body: auxEventBody}, function (err) {
        expect(err).toBe(null)

        augmenter(event, {}, function (err, message) {
          expect(err).toBe(null)

          s3.getObject({Bucket: bucketName, Key: 'flow/augmented/l337'}, function (err, obj) {
            expect(err).toBe(null)

            const augmentedRecord = JSON.parse(String(obj.Body))
            expect(augmentedRecord.data[0].taxes).toEqual(0.45)
            done()
          })
        })
      })
    })
  })
})
