'use strict'
const environment = process.env.ENVIRONMENT || 'docker'
const inputEventType = process.env.INPUT_EVENT_TYPE
const outputEventType = process.env.OUTPUT_EVENT_TYPE

const async = require('async')
const LambdaContext = require('node-helpers').LambdaContext
const Cache = require('node-helpers').Cache
const mySqlConnection = require('node-helpers').MysqlConnection

const config = require('./config/' + environment)
const InputEventHandler = require('node-helpers')['FlowInput' + inputEventType.toUpperCase()]
const OutputEventHandler = require('node-helpers')['FlowOutput' + outputEventType.toUpperCase()]
const compressOutput = process.env.COMPRESS_OUTPUT !== undefined ? !!process.env.COMPRESS_OUTPUT : config.compressOutput
const campaignsCf = require('./modules/campaignsConfiguration')
const augmentTasks = require('./modules/augmentTasks')

const cache = new Cache({
  hosts: config.cache.hosts,
  options: config.cache.options,
  ttl: process.env.CACHE_TTL || config.cache.ttl,
  prefix: process.env.CACHE_PREFIX || config.cache.prefix,
  enabled: process.env.CACHE !== undefined ? !!Number(process.env.CACHE) : config.cache.enabled
})

const connection = mySqlConnection.createConnection(config.mysql)
const campaignsConfiguration = new campaignsCf.CampaignsConfiguration(connection)

// Metrics
const NodeHelpersDDLogger = require('node-helpers').DDLogger
const DDLogger = new NodeHelpersDDLogger({
  namespace: config.stats.namespace,
  defaultTags: config.stats.defaultTags
})

function _augment (record, completed) {
  async.waterfall(
    [
      async.constant({
        record: record,
        cache: cache,
        campaignsConfiguration: campaignsConfiguration
      }),
      augmentTasks.init,
      augmentTasks.fetchFromCache,
      augmentTasks.fetchFromDatabase,
      augmentTasks.calculateEarn,
      augmentTasks.calculateRebate,
      augmentTasks.calculateTaxes
    ],
    function (err, params) {
      if (err) {
        return completed(err)
      }

      return completed(null, params.record)
    })
}

/**
 * Records augmenter
 *
 * This will complete missing record data with data
 * fetched from the storage (database, cache, etc)
 *
 * Works as follows:
 * - A batch of records is downloaded from S3
 * - Tasks for each record are created:
 *   - Fetch from the storage
 *   - Augment record
 * - Run created tasks in a pool (limit concurrency) in parallel
 * - Merge all augmented records into one batch
 * - Upload batch to S3
 *
 * If the campaign's hash for a record is not found in
 * the database, then that record is skipped, meaning
 * no error is passed to the callback. Otherwise it would
 * create an infinite retry cycle
 *
 * @param {object} event - Event that triggered for this function.
 * @param {object} context - Old API param replaced by callback (do not use).
 * @param {function} callback - Callback: function (err) {}
 * @public
 */
exports.handler = function (event, context, callback) {
  context.callbackWaitsForEmptyEventLoop = false

  const lambdaContext = new LambdaContext(context)
  const input = new InputEventHandler(event)

  let logPrefix = `[Version: ${lambdaContext.version}][BatchId: ${input.originalBatchId}]`

  async.waterfall(
    [
      function (next) {
        console.log(`${logPrefix} Parsing input data...`)
        input.parse(function inputDataParsed (err) {
          logPrefix += `[BatchId: ${input.originalBatchId}]`
          if (err) {
            console.log(`${logPrefix} Error while parsing ${inputEventType} input`, err.stack)
            DDLogger.increment('errors', { tags: { step: 'augmenter', reason: 'input-parse' } })
            return next(`${logPrefix} An error has occurred while trying to parse the ${inputEventType} input`)
          }
          console.log(`${logPrefix} The parsed ${inputEventType} input data size is ${input.messageSize} bytes (original: ${input.messageOriginalSize})`)
          DDLogger.gauge('input_size', input.messageSize)

          return next(null, input.data)
        })
      },
      function (records, next) {
        async.mapLimit(
          records,
          config.app.concurrencyLimit,
          async.reflect(_augment),
          function (_, results) {
            // Skip not-found errors, since we don't
            // want to retry for those (infinite loop)
            const errors = results
              .filter(result => (
                !!result.error &&
                !(result.error instanceof campaignsCf.errors.NotFoundError) &&
                !(result.error instanceof augmentTasks.errors.NoWinnerError)
              ))
              .map(result => result.error)

            const records = results
              .filter(result => !result.error)
              .map(result => result.value)

            if (errors.length) {
              console.log(errors)
              DDLogger.increment('errors', { tags: { step: 'augmenter', reason: 'fetch-records' } })
              return next('an error has occurred while fetching records ' + JSON.stringify(errors))
            }

            return next(null, records)
          })
      },
      function (outputMessage, next) {
        if (!outputMessage.length) {
          return next()
        }

        // Output dispatch
        console.log(`${logPrefix} Dispatching ${outputMessage.length} records`)

        const output = new OutputEventHandler(input, Object.assign({}, config[outputEventType.toLowerCase()], { compressOutput: compressOutput }))
        output.dispatch(outputMessage, function (err, result) {
          if (err) {
            console.log(err, err.stack)
            DDLogger.increment('errors', { tags: { step: 'augmenter', reason: 'dispatch' } })
            return next(`${logPrefix} An error has occurred while trying to dispatch the ${outputEventType} output`)
          }
          DDLogger.gauge('output_size', output.messageSize)
          console.log(`${logPrefix} Successfully dispatched ${outputEventType} ${output.getOutputName()}, with size of ${Buffer.byteLength(JSON.stringify(outputMessage), 'utf8')} bytes`)
          return next(null, result)
        })
      }
    ],
    function (err, result) {
      return callback(err, result)
    }
  )
}

if (environment !== 'prod') {
  exports.testsHelpers = {
    mysqlConnection: connection
  }
}
