'use strict'

describe('Aggregators Integration Test Suite', function () {
  const AWS = require('aws-sdk')
  const curated = require('../mock-records/curated.json')
  const sns = new AWS.SNS()
  const s3 = new AWS.S3()
  const utils = require('../../../../spec/utils')
  const lambdaBaseArn = 'arn:aws:lambda:us-east-1:123456789012:function:'
  const topicName = 'foo_test'
  const bucketName = 'flow-docker.mobra.in'
  const workers = {
    Quickstats: 'Quickstats',
    Cube_ByPublisher: 'Cube_ByPublisher',
    Cube_ByCarrier: 'Cube_ByCarrier',
    Cube_ByGeo: 'Cube_ByGeo'
  }

  const channel = {
    S3: 'S3',
    SNS: 'SNS'
  }

  jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000
  process.env.ENVIRONMENT = 'docker'

  let topicArn

  const snsEvent = {
    Records: [
      {
        EventVersion: '1.0',
        EventSource: 'aws:sns',
        Sns: {
          SignatureVersion: '1',
          Timestamp: '1970-01-01T00:00:00.000Z',
          Signature: 'EXAMPLE',
          SigningCertUrl: 'EXAMPLE',
          MessageId: '95df01b4-ee98-5cb9-9903-4c221d41eb5e',
          Message: JSON.stringify(curated),
          MessageAttributes: {
            Test: {
              Type: 'String',
              Value: 'TestString'
            },
            TestBinary: {
              Type: 'Binary',
              Value: 'TestBinary'
            }
          },
          Type: 'Notification',
          UnsubscribeUrl: 'EXAMPLE',
          TopicArn: 'arn:aws:sns:us-east-1:123456789012:flow-docker-{event}-curated',
          Subject: 'TestInvoke'
        }
      }
    ]
  }

  const s3event = {
    Records: [{
      s3: {
        bucket: {
          name: bucketName
        },
        object: {
          key: 'conversions/curated/foo'
        }
      }
    }]
  }

  const setEnv = (worker, input, output) => {
    process.env.WORKER_NAME = worker
    process.env.INPUT_EVENT_TYPE = input
    process.env.OUTPUT_EVENT_TYPE = output
  }

  beforeAll(function (done) {
    s3.createBucket({Bucket: bucketName}, function (err) {
      sns.createTopic({Name: topicName}, function (err, data) {
        topicArn = data.TopicArn
        done()
      })
    })
  })

  afterAll(function (done) {
    utils.aws.emptyBucket(bucketName, function (err) {
      sns.deleteTopic({TopicArn: topicArn}, function (err) {
        done()
      })
    })
  }, 2000)

  afterEach(function (done) {
    delete require.cache[require.resolve('../../index.js')]
    done()
  })

  it('should aggregate info to the curated record with Quickstats from SNS to SNS', (done) => {
    setEnv(workers.Quickstats, channel.SNS, channel.SNS)
    const expectedResult = require('../mock-records/quickstats.json')
    const aggregator = require('../..').handler
    aggregator(snsEvent, {invokedFunctionArn: lambdaBaseArn + 'flow-docker-conversions-aggregator:test'}, function (err, data) {
      expect(err).toBe(null)
      expect(data.message).toEqual(JSON.stringify(expectedResult))
      done()
    })
  }, 2000)

  it('should aggregate info to the curated record with Cube_ByPublisher from SNS to SNS', function (done) {
    setEnv(workers.Cube_ByPublisher, channel.SNS, channel.SNS)
    const expectedResult = require('../mock-records/bypublisher.json')
    const aggregator = require('../..').handler
    aggregator(snsEvent, {invokedFunctionArn: lambdaBaseArn + 'flow-docker-conversions-aggregator:test'}, function (err, data) {
      expect(err).toBe(null)
      expect(data.message).toEqual(JSON.stringify(expectedResult))
      done()
    })
  }, 2000)

  it('should aggregate info to the curated record with Cube_ByGeo from SNS to SNS', function (done) {
    setEnv(workers.Cube_ByGeo, channel.SNS, channel.SNS)
    const expectedResult = require('../mock-records/bygeo.json')
    const aggregator = require('../..').handler
    aggregator(snsEvent, {invokedFunctionArn: lambdaBaseArn + 'flow-docker-conversions-aggregator:test'}, function (err, data) {
      expect(err).toBe(null)
      expect(data.message).toEqual(JSON.stringify(expectedResult))
      done()
    })
  }, 2000)

  it('should aggregate info to the curated record with Cube_ByCarrier from SNS to SNS', function (done) {
    setEnv(workers.Cube_ByCarrier, channel.SNS, channel.SNS)
    const expectedResult = require('../mock-records/bycarrier.json')
    const aggregator = require('../..').handler
    aggregator(snsEvent, {invokedFunctionArn: lambdaBaseArn + 'flow-docker-conversions-aggregator:test'}, function (err, data) {
      expect(err).toBe(null)
      expect(data.message).toEqual(JSON.stringify(expectedResult))
      done()
    })
  }, 2000)

  it('should aggregate info to the curated record with Quickstats from S3 to S3', (done) => {
    setEnv(workers.Quickstats, channel.S3, channel.S3)
    const expectedResult = require('../mock-records/quickstats.json')
    const aggregator = require('../..').handler
    s3.putObject({Bucket: bucketName, Key: 'conversions/curated/foo', Body: JSON.stringify(curated)}, function (err) {
      aggregator(s3event, {invokedFunctionArn: lambdaBaseArn + 'flow-docker-conversions-curator:test'}, function (err, data) {
        expect(err).toBe(null)
        s3.getObject({Bucket: bucketName, Key: 'conversions/aggregated/quickstats/foo'}, function (err, data) {
          expect(data.Body.toString()).toEqual(JSON.stringify(expectedResult))
          done()
        })
      })
    })
  }, 2000)

  it('should aggregate info to the curated record with Cube_ByPublisher from S3 to S3', function (done) {
    setEnv(workers.Cube_ByPublisher, channel.S3, channel.S3)
    const expectedResult = require('../mock-records/bypublisher.json')
    const aggregator = require('../..').handler
    s3.putObject({Bucket: bucketName, Key: 'conversions/curated/foo', Body: JSON.stringify(curated)}, function (err) {
      aggregator(s3event, {invokedFunctionArn: lambdaBaseArn + 'flow-docker-conversions-curator:test'}, function (err, data) {
        expect(err).toBe(null)
        s3.getObject({Bucket: bucketName, Key: 'conversions/aggregated/cube-bypublisher/foo'}, function (err, data) {
          expect(data.Body.toString()).toEqual(JSON.stringify(expectedResult))
          done()
        })
      })
    })
  }, 2000)

  it('should aggregate info to the curated record with Cube_ByGeo from S3 to S3', function (done) {
    setEnv(workers.Cube_ByGeo, channel.S3, channel.S3)
    const expectedResult = require('../mock-records/bygeo.json')
    const aggregator = require('../..').handler
    s3.putObject({Bucket: bucketName, Key: 'conversions/curated/foo', Body: JSON.stringify(curated)}, function (err) {
      aggregator(s3event, {invokedFunctionArn: lambdaBaseArn + 'flow-docker-conversions-curator:test'}, function (err, data) {
        expect(err).toBe(null)
        s3.getObject({Bucket: bucketName, Key: 'conversions/aggregated/cube-bygeo/foo'}, function (err, data) {
          expect(data.Body.toString()).toEqual(JSON.stringify(expectedResult))
          done()
        })
      })
    })
  }, 2000)

  it('should aggregate info to the curated record with Cube_ByCarrier from S3 to S3', function (done) {
    setEnv(workers.Cube_ByCarrier, channel.S3, channel.S3)
    const expectedResult = require('../mock-records/bycarrier.json')
    const aggregator = require('../..').handler
    s3.putObject({Bucket: bucketName, Key: 'conversions/curated/foo', Body: JSON.stringify(curated)}, function (err) {
      aggregator(s3event, {invokedFunctionArn: lambdaBaseArn + 'flow-docker-conversions-curator:test'}, function (err, data) {
        expect(err).toBe(null)
        s3.getObject({Bucket: bucketName, Key: 'conversions/aggregated/cube-bycarrier/foo'}, function (err, data) {
          expect(data.Body.toString()).toEqual(JSON.stringify(expectedResult))
          done()
        })
      })
    })
  }, 2000)
})
