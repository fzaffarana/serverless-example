'use strict'

var workerConfPath = function (configName) {
  return '../workers_config/' + configName + '.json'
}

describe('WorkerConfig Unit Test Suite', function () {
  var mockery = require('mockery')
  var WorkerConfig = require('../../../modules/WorkerConfig')

  var defaultConfigName = 'Cube_ByMock'
  var defaultParentConfigName = 'CubeMock'

  var CubeMock = {
    baseUniqueFields: [
      'campaignId',
      'lineId'
    ],
    baseIncrements: {
      impressions: {
        init: 0,
        type: 'integer'
      }
    }
  }

  var Cube_byMock = {
    parentConfig: 'CubeMock',
    collection: 'byBrowser',
    mappings: {
      browserVersion: 'version'
    },
    uniqueFields: [
      'browser',
      'version'
    ]
  }

  beforeEach(function () {
    mockery.enable({useCleanCache: true})
    mockery.registerMock(workerConfPath(defaultConfigName), Cube_byMock)
    mockery.registerMock(workerConfPath(defaultParentConfigName), CubeMock)
  })

  afterEach(function () {
    mockery.deregisterAll()
    mockery.disable()
  })

  it('should load a worker config', function () {
    var workerConfig = new WorkerConfig(defaultConfigName)
    expect(workerConfig.increments).toBeDefined()

    // Child
    expect(workerConfig.parentConfig).toBeDefined()
    expect(workerConfig.collection).toBeDefined()
    expect(workerConfig.mappings).toBeDefined()
    expect(workerConfig.uniqueFields).toBeDefined()

    // Parent
    expect(workerConfig.baseUniqueFields).toBeDefined()
    expect(workerConfig.baseIncrements).toBeDefined()
  })

  it('should not modify the imported (json) object', function () {
    new WorkerConfig(defaultConfigName)
    expect(CubeMock.increments).not.toBeDefined()
    expect(CubeMock.uniqueFields).not.toBeDefined()
  })

  it('should throw on config not found', function () {
    mockery.registerAllowable(workerConfPath('Not_a_worker_config'))

    var badWorkerConfig = function () {
      new WorkerConfig('Not_a_worker_config')
    }
    expect(badWorkerConfig).toThrow()
  })

  it('should include child config', function () {
    var workerConfig = new WorkerConfig(defaultConfigName)
    expect(workerConfig.parentConfig).toEqual(Cube_byMock.parentConfig)
    expect(workerConfig.collection).toEqual(Cube_byMock.collection)
    expect(workerConfig.mappings).toEqual(Cube_byMock.mappings)
    expect(workerConfig.uniqueFields).toEqual(Cube_byMock.uniqueFields
      .concat(CubeMock.baseUniqueFields))
  })

  it('should include parent config', function () {
    var workerConfig = new WorkerConfig(defaultConfigName)
    expect(workerConfig.baseUniqueFields).toEqual(CubeMock.baseUniqueFields)
    expect(workerConfig.baseIncrements).toEqual(CubeMock.baseIncrements)
  })
})
