'use strict'

module.exports = {
  aws: {
    endpoint: 'http://localstack:4572',
    accessKeyId: 'foobar',
    secretAccessKey: 'foobar',
    region: 'us-east-1'
  },
  compressOutput: false,
  sns: {
    topicArn: 'arn:aws:sns:us-east-1:094103223014:flow-docker-{event}-aggregated-{worker_name}'
  },
  s3: {
    bucket: 'flow-docker.mobra.in',
    outputNameRules: {
      'curated': 'aggregated/{worker_name}'
    }
  },
  multiple: {
    dna: [
      'Dna_All',
      'Dna_ByAdSlot',
      'Dna_ByConnectionType',
      'Dna_ByDeviceType',
      'Dna_ByFormat',
      'Dna_ByGeo',
      'Dna_ByIsp',
      'Dna_ByOs',
      'Dna_Optimization',
      'Dna_Optimization2'
    ]
  },
  stats: {
    defaultTags: {
      environment: 'docker',
      application: 'docker-flow-aggregators'
    },
    namespace: 'docker-flow-aggregators'
  }
}
