'use strict'

module.exports = {
  aws: {
    region: 'us-east-1'
  },
  compressOutput: true,
  sns: {
    topicArn: 'arn:aws:sns:us-east-1:094103223014:flow-beta-{event}-aggregated-{worker_name}'
  },
  s3: {
    bucket: 'flow-beta.mobra.in',
    outputNameRules: {
      'curated': 'aggregated/{worker_name}'
    }
  },
  multiple: {
    dna: [
      'Dna_All',
      'Dna_ByAdSlot',
      'Dna_ByConnectionType',
      'Dna_ByDeviceType',
      'Dna_ByFormat',
      'Dna_ByGeo',
      'Dna_ByIsp',
      'Dna_ByOs',
      'Dna_Optimization',
      'Dna_Optimization2'
    ]
  },
  stats: {
    defaultTags: {
      environment: 'beta',
      application: 'beta-flow-aggregators'
    },
    namespace: 'beta-flow-aggregators'
  }
}
