'use strict'

var tools = require('node-helpers').tools

function WorkerConfig (workerName) {
  var workerConfig = tools.clone(require('../workers_config/' + workerName + '.json'))

  if (workerConfig.parentConfig) {
    var parentConfig = tools.clone(require('../workers_config/' + workerConfig.parentConfig + '.json'))
    workerConfig = Object.assign(parentConfig, workerConfig)
  }

  // Default fields

  workerConfig.uniqueFields = workerConfig.uniqueFields || []
  workerConfig.baseUniqueFields = workerConfig.baseUniqueFields || []

  workerConfig.increments = workerConfig.increments || {}
  workerConfig.baseIncrements = workerConfig.baseIncrements || {}

  // Merges

  workerConfig.uniqueFields = workerConfig.uniqueFields.concat(workerConfig.baseUniqueFields)

  workerConfig.increments = Object.assign({}, workerConfig.baseIncrements, workerConfig.increments)

  Object.assign(this, workerConfig)
}

module.exports = WorkerConfig
