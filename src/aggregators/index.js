'use strict'

// Environment settings
const environment = process.env.ENVIRONMENT || 'docker'
const workerName = process.env.WORKER_NAME
const workerGroupName = workerName || process.env.WORKER_GROUP_NAME.toLowerCase()
const inputEventType = process.env.INPUT_EVENT_TYPE
const outputEventType = process.env.OUTPUT_EVENT_TYPE
const config = require('./config/' + environment)
const compressOutput = process.env.COMPRESS_OUTPUT !== undefined ? !!process.env.COMPRESS_OUTPUT : config.compressOutput

// Vendors
const Profiler = require('node-helpers').Profiler
const LambdaContext = require('node-helpers').LambdaContext
const CuratedEventRecordsBag = require('node-helpers').CuratedEventRecordsBag
const InputEventHandler = require('node-helpers')['FlowInput' + inputEventType.toUpperCase()]
const OutputEventHandler = require('node-helpers')['FlowOutput' + outputEventType.toUpperCase()]

// Modules
const WorkerConfig = require('./modules/WorkerConfig')

const profiler = new Profiler({ enabled: !!Number(process.env.PROFILING_ENABLED) })

// Global vars
const workerConfigs = {}
if (workerName) {
  workerConfigs[workerName] = new WorkerConfig(workerName)
} else {
  config.multiple[workerGroupName].forEach(function (auxWorkerName) {
    workerConfigs[auxWorkerName] = new WorkerConfig(auxWorkerName)
  })
}

// Metrics
const NodeHelpersDDLogger = require('node-helpers').DDLogger
const DDLogger = new NodeHelpersDDLogger({
  namespace: config.stats.namespace,
  defaultTags: config.stats.defaultTags
})

exports.handler = function (event, context, cb) {
  context.callbackWaitsForEmptyEventLoop = false

  const lambdaContext = new LambdaContext(context)
  const input = new InputEventHandler(event)

  let logPrefix = `[Version: ${lambdaContext.version}]`

  // Parse the input message/data
  console.log(`${logPrefix} Parsing input data...`)
  profiler.startTimer('parse')
  let init = Date.now()
  input.parse(function inputDataParsed (err) {
    DDLogger.gauge('parse_timing', Date.now() - init)
    profiler.stopTimer('parse')

    logPrefix += `[BatchId: ${input.originalBatchId}]`

    if (err) {
      console.log(`${logPrefix} Error while parsing ${inputEventType} input`, err.stack)
      const message = `${logPrefix} An error has occurred while trying to parse the ${inputEventType} input`
      DDLogger.increment('errors', { tags: { step: 'aggregator', reason: 'input-parse' } })
      return cb(message)
    }

    console.log(`${logPrefix} The parsed input data size is ${input.messageSize} bytes (original: ${input.messageOriginalSize})`)
    DDLogger.gauge('input_size', input.messageSize)

    // Map and aggregate each worker config
    const outputMessages = {}
    for (let auxWorkerName in workerConfigs) {
      const workerConfig = workerConfigs[auxWorkerName]

      // Mapping
      console.log(`${logPrefix} Mapping the curated event records...`)
      const records = new CuratedEventRecordsBag(input.data, workerConfig.mappings)

      // Aggregation
      profiler.startTimer('aggregate')
      init = Date.now()
      console.log(`${logPrefix} Aggregating records according to worker ${workerGroupName}...`)
      const aggregatedRecords = records.aggregate({
        groupBy: workerConfig.uniqueFields,
        increment: workerConfig.increments
      })
      profiler.stopTimer('aggregate')
      DDLogger.gauge('aggregation_timing', Date.now() - init)

      // Output data generation
      const outputMessage = []
      for (let id in aggregatedRecords) {
        outputMessage.push(Object.assign({}, { _id: id }, aggregatedRecords[id]))
      }
      outputMessages[auxWorkerName] = outputMessage
    }

    // Output dispatch
    const output = new OutputEventHandler(input, Object.assign({}, config[outputEventType.toLowerCase()], { workerName: workerGroupName, compressOutput: compressOutput }))
    output.dispatch(workerName ? outputMessages[workerName] : outputMessages, function (err, result) {
      if (err) {
        console.log(err, err.stack)
        const message = `${logPrefix} An error has occurred while trying to dispatch the ${outputEventType} output`
        DDLogger.increment('errors', { tags: { step: 'aggregator', reason: 'dispatch' } })
        return cb(message)
      }
      DDLogger.gauge('output_size', output.messageSize)
      console.log(`${logPrefix} Successfully dispatched ${outputEventType} ${output.getOutputName()}, with size of ${output.messageSize} bytes`)
      return cb(null, result)
    })
  })
}
