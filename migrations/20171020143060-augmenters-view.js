'use strict'

exports.up = function (db) {
  return db.createTable('flow_augmenters_view', {
    campaignId: {type: 'int', notNull: true, unsigned: true},
    lineId: {type: 'int', notNull: true, unsigned: true},
    orderId: {type: 'int', notNull: true, unsigned: true},
    clientId: {type: 'int', notNull: true, unsigned: true},
    sourceId: {type: 'int', notNull: true, unsigned: true},
    creativeId: {type: 'int', notNull: true, unsigned: true},
    accountManagerId: {type: 'int', notNull: true, unsigned: true},
    salesManagerId: {type: 'int', notNull: true, unsigned: true},
    adopsManagerId: {type: 'int', notNull: true, unsigned: true},
    departmentId: {type: 'smallint', notNull: false, unsigned: true, length: 5},
    labelId: {type: 'smallint', unsigned: true, length: 5},
    sellingModeId: {type: 'int', notNull: true, unsigned: true},
    buyingModeId: {type: 'int', notNull: true, unsigned: true},
    agencyId: {type: 'int', notNull: false, unsigned: true},
    productId: {type: 'int', notNull: true, unsigned: true},
    sellingPayout: {type: 'bigint', length: 20, notNull: false},
    buyingFinancialEntityId: {type: 'int', notNull: false, unsigned: true},
    sellingFinancialEntityId: {type: 'int', notNull: false, unsigned: true},
    sourceAccountManagerId: {type: 'int', notNull: false, unsigned: true},
    orderRebate: {type: 'float', defaultValue: null, unsigned: true},
    orderTaxes: {type: 'float', defaultValue: null, unsigned: true},
    sourceType: {type: 'varchar', defaultValue: null, length: 2000},
    clientType: {type: 'varchar', defaultValue: null, length: 2000},
    lineType: {type: 'varchar', defaultValue: null, length: 2000},
    hash: {type: 'varchar', notNull: true, length: 90}
  })

  exports.down = function (db) {
    return db.dropTable('flow_augmenters_view')
  }

  exports._meta = {
    'version': 1
  }
}