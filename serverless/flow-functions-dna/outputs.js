'use strict'

const utils = require('../utils')

const lambdaOutputs = [
  {fnName: 'clicks-dna-curator'},
  {fnName: 'impressions-dna-aggregator-dna'},
  {fnName: 'clicks-dna-aggregator-dna'}
]

module.exports.default = function () {
  let outputs = {}

  // Add the Lambdas Alarms
  lambdaOutputs.forEach(function (outputSettings) {
    const dashName = utils.ucfirst(outputSettings.fnName.split('-').join('Dash')) + 'LambdaFunction'
    const capitalizedName = outputSettings.fnName.split('-').map(name => utils.capitalize(name)).join('')

    // Export ARN
    outputs[capitalizedName + 'LambdaArnExport'] = {
      Value: {
        'Fn::GetAtt': [dashName, 'Arn']
      },
      Export: {
        Name: '${self:custom.exportName}-arn-' + outputSettings.fnName + '-lambda'
      }
    }
  })

  return outputs
}
