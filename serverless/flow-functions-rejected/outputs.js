'use strict'

const utils = require('../utils')

const lambdaOutputs = [
  {fnName: 'clicks-rejected-curator'},
  {fnName: 'conversions-rejected-curator'},
  {fnName: 'clicks-rejected-aggregator-quickstats'},
  {fnName: 'conversions-rejected-aggregator-quickstats'}
]

module.exports.default = function () {
  let outputs = {}

  // Add the Lambdas Alarms
  lambdaOutputs.forEach(function (outputSettings) {
    const dashName = utils.ucfirst(outputSettings.fnName.split('-').join('Dash')) + 'LambdaFunction'
    const capitalizedName = outputSettings.fnName.split('-').map(name => utils.capitalize(name)).join('')

    // Export ARN
    outputs[capitalizedName + 'LambdaArnExport'] = {
      Value: {
        'Fn::GetAtt': [dashName, 'Arn']
      },
      Export: {
        Name: '${self:custom.exportName}-arn-' + outputSettings.fnName + '-lambda'
      }
    }
  })

  return outputs
}
