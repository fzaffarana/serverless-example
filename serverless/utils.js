'use strict'

module.exports = {

  ucfirst: function _ucfirst (string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
  },

  capitalize: function _capitalize (string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
  }

}
