'use strict'

const utils = require('../utils')

const topics = [
  'impressions-augmented',
  'impressions-raw',
  'clicks-raw',
  'clicks-dna-raw',
  'clicks-rejected-raw',
  'conversions-raw',
  'conversions-rejected-raw',
  'impressions-curated',
  'clicks-curated',
  'clicks-dna-curated',
  'clicks-rejected-curated',
  'conversions-curated',
  'conversions-rejected-curated'
]

function getOutputs (suffix) {
  suffix = suffix || ''

  let outputs = {}

  // Add the Lambdas Alarms
  topics.forEach(function (topicName) {
    console.log(topicName)
    topicName = topicName + suffix
    const capitalizedName = topicName.split('-').map(name => utils.capitalize(name)).join('')

    // Export ARN
    outputs[capitalizedName + 'TopicExport'] = {
      Value: {
        Ref: capitalizedName + 'Topic'
      },
      Export: {
        Name: '${self:custom.exportName}-arn-' + topicName + '-topic'
      }
    }
  })

  return outputs
}

module.exports.default = function () {
  let outputs = {}

  Object.assign(outputs, getOutputs())

  // DLQ Outputs
  outputs['FlowDLQNameExport'] = {
    Value: {
      'Fn::GetAtt': ['FlowDLQ', 'QueueName']
    },
    Export: {
      Name: '${self:custom.exportName}-name-dlq'
    }
  }
  outputs['FlowDLQURLExport'] = {
    Value: {
      Ref: 'FlowDLQ'
    },
    Export: {
      Name: '${self:custom.exportName}-url-dlq'
    }
  }

  return outputs
}
