'use strict'

var yaml = require('js-yaml')
var fs = require('fs')

const utils = require('../../utils')

var path = require.resolve('./serverless_resources.yml')
var config = yaml.safeLoad(fs.readFileSync(path, 'utf8'))

/**
* This will swap the default bucket reference
* by a reference containing the stage.
*
* For example:
*   S3BucketFlowmobrain: ...
* Becomes:
*   S3BucketFlowprodmobrain: ...
*
* @param {string} stage - Stage
* @return {object} - Content of yml resource.Resources
*/
function initStageResources (stage) {
  var bucketName = 'S3BucketFlowmobrain'
  var bucketStageName = `S3BucketFlow${stage}mobrain`
  var resources = config.resources.Resources

  // Swap bucketName by bucketStageName
  resources[bucketStageName] = resources[bucketName]
  delete resources[bucketName]

  return resources
}

var topics = [

  'clicks-raw',
  'clicks-dna-raw',
  'clicks-rejected-raw',
  'conversions-raw',
  'conversions-rejected-raw',
  'impressions-raw',

  'impressions-augmented',

  'clicks-curated',
  'clicks-dna-curated',
  'clicks-rejected-curated',
  'conversions-curated',
  'conversions-rejected-curated',
  'impressions-curated',

  'clicks-aggregated-quickstats',
  'conversions-aggregated-quickstats',
  'impressions-aggregated-quickstats',

  'clicks-dna-aggregated-dna',
  'impressions-dna-aggregated-dna',

  'clicks-dna-aggregated-dna-all',
  'impressions-dna-aggregated-dna-all',

  'clicks-rejected-aggregated-quickstats',
  'conversions-rejected-aggregated-quickstats'
]

function getTopics (suffix) {
  suffix = suffix || ''

  var topicsResources = {}

  topics.forEach(function (topicName) {
    topicName = topicName + suffix
    const capitalizedName = topicName.split('-').map(name => utils.capitalize(name)).join('') + 'Topic'

    topicsResources[capitalizedName] = {
      Type: 'AWS::SNS::Topic',
      Properties: {
        TopicName: 'flow-${self:provider.stage}-' + topicName
      }
    }
  })

  return topicsResources
}

const streams = [
  // 'clicks-raw'
]

function getStreams () {
  var streamsResources = {}

  streams.forEach(function (streamName) {
    const capitalizedName = streamName.split('-').map(name => utils.capitalize(name)).join('') + 'Stream'

    streamsResources[capitalizedName] = {
      Type: 'AWS::Kinesis::Stream',
      Properties: {
        Name: 'flow-${self:provider.stage}-' + streamName,
        RetentionPeriodHours: 24,
        ShardCount: 1,
        Tags: [{"Key": "application", "Value": "flow"}]
      }
    }
  })

  return streamsResources
}

function resources (stage) {
  return function () {
    var resources = initStageResources(stage)

    Object.assign(resources, getTopics())

    if (stage === 'prod') {
      Object.assign(resources, getStreams())
    }

    return resources
  }
}

module.exports = {
  test: resources('test'),
  beta: resources('beta'),
  stage: resources('stage'),
  prod: resources('prod')
}
