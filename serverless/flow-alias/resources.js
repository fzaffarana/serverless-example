'use strict'

const utils = require('../utils')

const commonVersions = {
  impressions: {augmenters: '$LATEST', curators: '$LATEST', aggregators: '$LATEST', aggregatorQuickstats: '$LATEST'},
  impressionsDna: {aggregators: '$LATEST'},
  clicks: {curators: '$LATEST', aggregators: '$LATEST', aggregatorQuickstats: '$LATEST'},
  clicksDna: {curators: '$LATEST', aggregators: '$LATEST'},
  clicksRejected: {curators: '$LATEST', aggregators: '$LATEST'},
  conversions: {curators: '$LATEST', aggregators: '$LATEST', aggregatorQuickstats: '$LATEST'},
  conversionsRejected: {curators: '$LATEST', aggregators: '$LATEST'}
}

const commonAliases = {
  prod: {
    'impressions-augmenter': {topic: 'impressions-raw', version: commonVersions.impressions.augmenters},
    'impressions-curator': {topic: 'impressions-augmented', version: commonVersions.impressions.curators},
    'clicks-curator': {topic: 'clicks-raw', version: commonVersions.clicks.curators},
    'clicks-dna-curator': {topic: 'clicks-dna-raw', version: commonVersions.clicksDna.curators},
    'clicks-rejected-curator': {topic: 'clicks-rejected-raw', version: commonVersions.clicksRejected.curators},
    'conversions-rejected-curator': {topic: 'conversions-rejected-raw', version: commonVersions.conversionsRejected.curators},
    'impressions-aggregator-quickstats': {topic: 'impressions-curated', version: commonVersions.impressions.aggregatorQuickstats},
    'clicks-aggregator-quickstats': {topic: 'clicks-curated', version: commonVersions.clicks.aggregatorQuickstats},
    'impressions-dna-aggregator-dna': {topic: 'impressions-curated', version: commonVersions.impressionsDna.aggregators},
    'clicks-dna-aggregator-dna': {topic: 'clicks-dna-curated', version: commonVersions.clicksDna.aggregators},
    'clicks-rejected-aggregator-quickstats': {topic: 'clicks-rejected-curated', version: commonVersions.clicksRejected.aggregators},
    'conversions-rejected-aggregator-quickstats': {topic: 'conversions-rejected-curated', version: commonVersions.conversionsRejected.aggregators}
  },
  beta: {
    'impressions-augmenter': {topic: 'impressions-raw', version: '$LATEST'},
    'impressions-curator': {topic: 'impressions-augmented', version: '$LATEST'},
    'clicks-curator': {topic: 'clicks-raw', version: '$LATEST'},
    'clicks-dna-curator': {topic: 'clicks-dna-raw', version: '$LATEST'},
    'impressions-aggregator-quickstats': {topic: 'impressions-curated', version: '$LATEST'},
    'clicks-aggregator-quickstats': {topic: 'clicks-curated', version: '$LATEST'},
    'impressions-dna-aggregator-dna': {topic: 'impressions-curated', version: '$LATEST'},
    'clicks-dna-aggregator-dna': {topic: 'clicks-dna-curated', version: '$LATEST'}
  }
}

const aliasesPerStage = {
  prod: {
    prod: commonAliases.prod
  },
  beta: {
    beta: commonAliases.beta
  },
  stage: {
    // Same as PROD, but change every version with $LATEST
    stage: Object.keys(commonAliases.prod).reduce(function (previous, current) {
      previous[current] = Object.assign({}, commonAliases.prod[current])
      previous[current].version = '$LATEST'
      return previous
    }, {})
  }
}

function resources (stage) {
  return function () {
    let resourcesObj = {}

    const aliases = aliasesPerStage[stage]
    Object.keys(aliases).forEach(function (alias) {
      const aliasFunctions = aliases[alias]
      Object.keys(aliasFunctions).forEach(function (fnName) {
        const capitalizedName = fnName.split('-').map(name => utils.capitalize(name)).join('') + utils.capitalize(alias)
        const version = aliasFunctions[fnName].version
        const topic = aliasFunctions[fnName].topic
        const stage = aliasFunctions[fnName].stage

        // Create the Alias
        resourcesObj[capitalizedName + 'FunctionAlias'] = {
          Type: 'AWS::Lambda::Alias',
          Properties: {
            Name: alias,
            Description: alias,
            FunctionName: {
              'Fn::ImportValue': `\${self:custom.imports.flow}-arn-${fnName}-lambda`
            },
            FunctionVersion: version
          }
        }

        // Create the Topic Permission
        resourcesObj[capitalizedName + 'TopicPermission'] = {
          DependsOn: [
            capitalizedName + 'FunctionAlias'
          ],
          Type: 'AWS::Lambda::Permission',
          Properties: {
            Action: 'lambda:InvokeFunction',
            Principal: 'sns.amazonaws.com',
            SourceArn: {
              'Fn::ImportValue': stage ? `flow-${stage}-export-arn-${topic}-topic` : `\${self:custom.imports.flow}-arn-${topic}-topic`
            },
            FunctionName: {
              'Fn::Join': [
                '',
                [
                  {'Fn::ImportValue': `\${self:custom.imports.flow}-arn-${fnName}-lambda`},
                  `:${alias}`
                ]
              ]
            }
          }
        }

        // Create the Topic Subscription
        resourcesObj[capitalizedName + 'TopicSubscription'] = {
          DependsOn: [
            capitalizedName + 'FunctionAlias',
            capitalizedName + 'TopicPermission'
          ],
          Type: 'AWS::SNS::Subscription',
          Properties: {
            Endpoint: {
              'Fn::Join': [
                '',
                [
                  {'Fn::ImportValue': `\${self:custom.imports.flow}-arn-${fnName}-lambda`},
                  `:${alias}`
                ]
              ]
            },
            Protocol: 'lambda',
            TopicArn: {
              'Fn::ImportValue': stage ? `flow-${stage}-export-arn-${topic}-topic` : `\${self:custom.imports.flow}-arn-${topic}-topic`
            }
          }
        }
      })
    })

    return resourcesObj
  }
}

module.exports = {
  beta: resources('beta'),
  stage: resources('stage'),
  prod: resources('prod')
}
