'use strict'

const utils = require('../utils')

function _refName (name, type) {
  name = name
    .split('-')
    .map(word => utils.capitalize(word))
    .join('')

  type = type
    .split(' ')
    .map(word => utils.capitalize(word))
    .join('')

  return `${name}Lambda${type}Alarm`
}

function _alarmName (functionName) {
  return functionName
    .split('-')
    .map(word => utils.capitalize(word))
    .join('_')
}

function _getAlarmObject (params) {
  params = params || {}

  let baseObj = {
    Type: 'AWS::CloudWatch::Alarm',
    Properties: {
      AlarmName: `Flow_\${self:provider.stage}_${params.alarmName}`,
      Namespace: params.namespace,
      Dimensions: params.dimensions,
      ActionsEnabled: '${self:custom.alarms.enabled.${self:provider.stage}, self:custom.alarms.enabled.default}',
      Unit: params.unit || 'Count',
      Statistic: params.statistic || 'Sum',
      MetricName: params.metricName,
      ComparisonOperator: params.comparisonOperator,
      Threshold: params.threshold,
      EvaluationPeriods: params.evaluationPeriods || 1,
      Period: params.period || 60
    }
  }
  if (params.alarmActions !== false) { // Default is TRUE
    baseObj.Properties.AlarmActions = ['${self:custom.alarms.arn}']
  }
  if (params.insufficientDataActions !== false) { // Default is TRUE
    baseObj.Properties.InsufficientDataActions = ['${self:custom.alarms.arn}']
  }
  if (params.okActions === true) { // Default is FALSE
    baseObj.Properties.OKActions = ['${self:custom.alarms.arn}']
  }

  return {[params.refName]: baseObj}
}

function _getLambdaAlarm (customParams) {
  customParams = customParams || {}

  let params = {
    namespace: 'AWS/Lambda',
    dimensions: [
      {
        Name: 'FunctionName',
        Value: {
          'Fn::Select': ['6', {'Fn::Split': [':', {'Fn::ImportValue': `\${self:custom.imports.functions}-arn-${customParams.functionName}-lambda`}]}]
        }
      }
    ],
    evaluationPeriods: 10,
    period: 60
  }

  Object.assign(params, customParams)

  return _getAlarmObject(params)
}

function LambdaAlarmInvocations (functionName, customParams) {
  let params = {
    refName: _refName(functionName, 'invocations'),
    functionName: functionName,
    alarmName: `${_alarmName(functionName)}_Invocations_ALT`,
    metricName: 'Invocations',
    comparisonOperator: 'LessThanOrEqualToThreshold',
    threshold: 1,
    evaluationPeriods: 10
  }

  return _getLambdaAlarm(Object.assign(params, customParams || {}))
}

function LambdaAlarmErrors (functionName, customParams) {
  let params = {
    refName: _refName(functionName, 'errors'),
    functionName: functionName,
    alarmName: `${_alarmName(functionName)}_Errors_ALT`,
    metricName: 'Errors',
    comparisonOperator: 'GreaterThanOrEqualToThreshold',
    threshold: 5,
    evaluationPeriods: 3
  }

  return _getLambdaAlarm(Object.assign(params, customParams || {}))
}

function LambdaAlarmDuration (functionName, customParams) {
  let params = {
    refName: _refName(functionName, 'duration'),
    functionName: functionName,
    alarmName: `${_alarmName(functionName)}_Duration_ALT`,
    unit: 'Milliseconds',
    statistic: 'Average',
    metricName: 'Duration',
    comparisonOperator: 'GreaterThanOrEqualToThreshold',
    threshold: 7000,
    evaluationPeriods: 10
  }

  return _getLambdaAlarm(Object.assign(params, customParams || {}))
}

function LambdaAlarmDeadLetterErrors (functionName, customParams) {
  let params = {
    refName: _refName(functionName, 'dead letter errors'),
    functionName: functionName,
    alarmName: `${_alarmName(functionName)}_DeadLetterErrors_ALT`,
    metricName: 'DeadLetterErrors',
    comparisonOperator: 'GreaterThanThreshold',
    threshold: 1,
    evaluationPeriods: 1,
    insufficientDataActions: false
  }

  return _getLambdaAlarm(Object.assign(params, customParams || {}))
}

const commonLambdaAlarms = [
  LambdaAlarmInvocations,
  LambdaAlarmErrors,
  // LambdaAlarmDuration,
  LambdaAlarmDeadLetterErrors
]

const commonImpressionsThresholds = {
  augmenter: {
    'LambdaAlarmDuration': {threshold: 35000}
  },
  curator: {
    'LambdaAlarmDuration': {threshold: 15000}
  },
  aggregatorDna: {
    'LambdaAlarmDuration': {threshold: 35000}
  }
}

const commonClicksThresholds = {
  aggregatorDna: {
    'LambdaAlarmDuration': {threshold: 15000}
  }
}

const commonConversionsThresholds = {
  'LambdaAlarmDuration': {threshold: 700}
}

const lambdaAlarms = [
  {fnName: 'impressions-augmenter', alarms: commonLambdaAlarms, params: commonImpressionsThresholds.augmenter},
  {fnName: 'impressions-curator', alarms: commonLambdaAlarms, params: commonImpressionsThresholds.curator},
  {fnName: 'clicks-curator', alarms: commonLambdaAlarms},
  {fnName: 'clicks-dna-curator', alarms: commonLambdaAlarms},
  {fnName: 'clicks-rejected-curator', alarms: commonLambdaAlarms},
  {fnName: 'conversions-rejected-curator', alarms: commonLambdaAlarms, params: Object.assign({}, commonConversionsThresholds, {'LambdaAlarmInvocations': {threshold: 1}})},
  {fnName: 'impressions-aggregator-quickstats', alarms: commonLambdaAlarms},
  {fnName: 'clicks-aggregator-quickstats', alarms: commonLambdaAlarms},
  {fnName: 'impressions-dna-aggregator-dna', alarms: commonLambdaAlarms, params: commonImpressionsThresholds.aggregatorDna},
  {fnName: 'clicks-dna-aggregator-dna', alarms: commonLambdaAlarms, params: commonClicksThresholds.aggregatorDna},
  {fnName: 'clicks-rejected-aggregator-quickstats', alarms: commonLambdaAlarms},
  {fnName: 'conversions-rejected-aggregator-quickstats', alarms: commonLambdaAlarms, params: Object.assign({}, commonConversionsThresholds, {'LambdaAlarmInvocations': {threshold: 1}})}
]

module.exports.default = function () {
  let resources = {}

  // Add the Lambdas Alarms
  lambdaAlarms.forEach(function (alarmSettings) {
    if (alarmSettings.alarms instanceof Array) {
      alarmSettings.alarms.forEach(function (alarmGenerator) {
        alarmSettings.params = alarmSettings.params || {}
        Object.assign(resources, alarmGenerator(alarmSettings.fnName, alarmSettings.params[alarmGenerator.name] || {}))
      })
    }
  })

  // Add the DLQ Alarm
  Object.assign(resources, _getAlarmObject({
    refName: 'DLQAlarm',
    alarmName: 'DeadLetterQueue_Messages_ALT',
    namespace: 'AWS/SQS',
    dimensions: [
      {
        Name: 'QueueName',
        Value: {
          'Fn::ImportValue': '${self:custom.imports.functions}-name-dlq'
        }
      }
    ],
    metricName: 'ApproximateNumberOfMessagesVisible',
    comparisonOperator: 'GreaterThanThreshold',
    threshold: 5,
    evaluationPeriods: 1,
    insufficientDataActions: false
  }))

  return resources
}
